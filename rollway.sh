#!/bin/sh

### BEGIN INIT INFO
# Provides:          rollway
# Required-Start:
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Run the Rollway log indexing service
# Description:       Rollway is notified when Blue Jeans client logs are uploaded. It indexes the meeting and endpoint GUIDs in the logs, as well as the endpoint type, email address, and timestamp of the upload. Flume will poll Rollway periodically for new files, which it will copy to Flume's storage and remove from Rollway's storage. Flume will then be able to link Indigo to the logs.
### END INIT INFO

. /lib/lsb/init-functions

[ -f /etc/default/rcS ] && . /etc/default/rcS
PROGRAM_DIR=
PROGRAM_NAME=Rollway
PROGRAM=`which node`
CONFIG_FILE="${PROGRAM_DIR}/config.json"
PID_FILE="${PROGRAM_DIR}/.pid"

test -d $PROGRAM_DIR || exit 0

NODE_ENV="production"
export NODE_ENV

start()
{
	log_daemon_msg "Starting $PROGRAM_NAME"
	start_daemon -p "$PID_FILE" "$PROGRAM" "$PROGRAM_DIR" --config "$CONFIG_FILE" &
	sleep 0.4
	Pid=`cat "$PID_FILE"`
	log_progress_msg "running with PID ${Pid}"
	log_end_msg 0
}

stop()
{
	pidofproc -p "$PID_FILE" "$PROGRAM" > /dev/null
	if [ "$?" = "0" ]; then
		log_daemon_msg "Stopping $PROGRAM_NAME"
		killproc -p "$PID_FILE" "$PROGRAM"
		log_progress_msg "stopped"
	else
		log_begin_msg "Not running"
	fi
	log_end_msg 0

	if [ "$?" = "1" ]; then
		`rm "$PID_FILE"`
	fi
}

status()
{
	status_of_proc -p "$PID_FILE" "$PROGRAM" "$PROGRAM_NAME"
}

case "$1" in
	start)
		start
		;;
	stop)	
		stop
		;;
	restart)
		stop
		start
		;;
	status)
		status
		;;
	*)
		echo "Usage: $0 {start|stop|restart|status}"
		exit 1
		;;
esac

exit 0