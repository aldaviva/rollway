define([
	'require',
	'./DetailsPane',
	'./FilterPane',
	'./GridPane',
	'./preferencesDialog',
	'dojo/router',
	'dojo/io-query',
	'dojo/parser',
	'dojo/on',
	'dojo/query',
	'dojo/ready'
], function(
	require,
	DetailsPane,
	FilterPane,
	GridPane,
	preferencesDialog,
	router,
	ioQuery,
	parser,
	on,
	query,
	ready
){

	ready(function(){
		parser.parse();
	});

	var detailsPane = new DetailsPane();
	detailsPane.placeAt("stage");

	var gridPane = new GridPane({ detailsPane: detailsPane });
	gridPane.placeAt("stage", "first");

	var filterPane = new FilterPane({ gridPane: gridPane });
	filterPane.placeAt("stage", "first");

	on(query('header .preferences a'), 'click', function(event){
		event.preventDefault();
		preferencesDialog.show();
	});

});