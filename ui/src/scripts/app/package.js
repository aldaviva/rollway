var profile = {
	resourceTags: {
		amd: function(filename, module){
			return /\.js$/.test(filename);
		},
		test: function(filename, module){
			return false;
		},
		copyOnly: function(filename, module){
			//probably doesn't do anything because our css is outside of scripts/app
			// return /\.css$/.test(filename);
			return false;
		},
		miniExclude: function(filename, module) {
			return module in {
				'app/package': 1
			};
		}
	}
};