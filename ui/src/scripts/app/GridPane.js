define([
	'require',
	'lodash/lodash', 
	'./bundleRepository',
	'dojo/data/ObjectStore',
	'dojox/grid/DataGrid',
	'dojo/date/locale',
	'dojo/_base/declare',
	'dojo/dom-construct',
	'dijit/_WidgetBase',
	'dojo/ready'
], function(
	require,
	_,
	bundleRepository,
	ObjectStore,
	DataGrid,
	locale,
	declare,
	domConstruct,
	_WidgetBase,
	ready)
{

	function dateFormatter(raw){
		return locale.format(new Date(raw*1000), { formatLength: 'short' }).toLowerCase();
	}

	function emailFormatter(raw){
		return raw || '\u2014';
	}

	return declare([_WidgetBase], {

		detailsPane: null,
		"class": "GridPane",

		buildRendering: function(){
			this.inherited(arguments);
			var el = this.domNode;

			domConstruct.create('h2', { innerHTML: 'Uploads' }, el);

			var grid = this.grid = new DataGrid({
				store: new ObjectStore({ objectStore: bundleRepository }),
				structure: [
					{ name: 'Email', field: 'email', width: '279px', formatter: emailFormatter },
					{ name: 'Date', field: 'timestamp', width: '130px', formatter: dateFormatter }
				],
				selectionMode: 'single',
				sortInfo: -2, //column two, descending
				selectable: true,
				autoWidth: false
			});

			grid.placeAt(el);
		},

		postCreate: function(){
			var detailsPane = this.detailsPane;
			var grid = this.grid;

			grid.on('rowclick', function(e){
				var selectedItem = grid.getItem(grid.selection.selectedIndex);

				detailsPane.set('bundleId', selectedItem._id);
			});

			window.addEventListener('resize', _.throttle(function(){
				grid.resize();
			}, 500), false);

			ready(function(){
				grid.startup();
			});
		}
	});
});