define([
	'lodash/lodash',
	'dojo/_base/declare',
	'dojo/dom-construct',
	'dijit/_WidgetBase',
	'dojo/date/locale',
	'dojo/dom-class',
	'dojo/io-query',
	'dojo/hash',
	'dojo/request',
	'dojo/on'],
function(
	_,
	declare,
	domConstruct,
	_WidgetBase,
	locale,
	domClass,
	ioQuery,
	hash,
	request,
	on)
{

	var DetailsPane = declare([_WidgetBase], {

		"class": "DetailsPane",

		keys: [
			{ id: 'comment',  label: 'Comments',  el: null, render: renderString   },
			{ id: 'files',    label: 'Log Files', el: null, render: renderFiles    },
			{ id: 'meetings', label: 'Meetings',  el: null, render: renderMeetings },
			{ id: 'legs',     label: 'Legs',      el: null, render: renderLegs     }
		],

		bundle: null,
		bundleId: null,
		placeholder: null,

		buildRendering: function(){
			this.inherited(arguments);
			var el = this.domNode;

			domConstruct.create('h2', { innerHTML: 'Details' }, el);

			this.placeholder = domConstruct.create('div', { innerHTML: 'Select an upload<br/>to see more information', 'class': 'placeholder' }, el);
			domConstruct.create('div', { "class": "icon" }, this.placeholder, "first");

			var dl = this.dl = domConstruct.create("dl", { "class": "content" }, el);
			this.keys.forEach(function(key){
				domConstruct.create("dt", { 'innerHTML': key.label }, dl);
				key.el = domConstruct.create("dd", null, dl);
			});

			on(_.where(this.keys, { id: 'files' })[0].el, "a:click", function(){
				request('analytics/event/event:client:fileViewed', { method: 'POST', preventCache: true });
			});

			on(_.where(this.keys, { id: 'meetings' })[0].el, "a:click", function(){
				request('analytics/event/event:client:indigoLinked', { method: 'POST', preventCache: true });
			});

			on(_.where(this.keys, { id: 'legs' })[0].el, "a:click", function(){
				request('analytics/event/event:client:indigoLinked', { method: 'POST', preventCache: true });
			});
		},

		_setBundleIdAttr: function(bundleId){
			this.bundleId = bundleId;

			domClass.toggle(this.placeholder, "hidden", !!bundleId);
			domClass.toggle(this.dl, "hidden", !bundleId);

			if(bundleId !== null){
				request('bundles/'+bundleId, { handleAs: 'json' })
					.then(_.bind(function(bundle){
						this.bundle = bundle;
						this.keys.forEach(function(key){
							var dd = key.el;
							var value = bundle[key.id];
							var node = key.render(value, bundle);
							domConstruct.place(node, dd, "only");
						});
					}, this));
			}

			var hashQueryObject = ioQuery.queryToObject(hash());
			if(bundleId !== null){
				hashQueryObject.bundle = bundleId;
			} else {
				delete hashQueryObject.bundle;
			}

			hash(ioQuery.objectToQuery(hashQueryObject));
		},

		postCreate: function(){
			this.onHashChange(hash(), true);
		},

		onHashChange: function(newHash, isFirstRun){
			var hashQueryObject = ioQuery.queryToObject(newHash);

			if(isFirstRun || (hashQueryObject.bundle != this.get('bundleId'))){

				if(hashQueryObject.bundle !== undefined){
					this.set('bundleId', hashQueryObject.bundle);
				} else {
					this.set('bundleId', null);
				}
			}
		}
	});

	function renderDate(raw){
		var string = locale.format(new Date(raw*1000), { formatLength: 'full' });
		return renderString(string);
	}

	function renderString(raw){
		return document.createTextNode(raw.toString());
	}

	function renderFiles(items, bundle){
		var list = new FileList(items, bundle);
		return list.render();
	}

	function renderMeetings(items, bundle){
		var list = new IndigoLinkList(items, "http://10.4.4.251:2645/indaco.html?meeting=");
		return list.render();
	}

	function renderLegs(items, bundle){
		var list = new IndigoLinkList(items, "http://10.4.4.251:2645/indaco.html?endpoint=");
		return list.render();
	}

	var LinkList = declare(null, {
		constructor: function(items){
			_.bindAll(this);

			this.items = _.sortBy(_.flatten([items], true), this.sortComparator);
		},

		render: function(){
			var el;

			if(this.items.length > 0){
				el = domConstruct.create('ul');

				_.each(this.items, function(item){
					var li = domConstruct.create('li', {}, el);
					this.renderListItem(li, item);
				}, this);
			} else {
				el = domConstruct.create("em", { innerHTML: "none" });
			}

			return el;
		},

		renderListItem: function(li, data){
			throw "abstract LinkList.renderListItem";
		},

		renderLink: function(href, text){
			var a = domConstruct.create('a', { href: href, target: '_blank' });
			domConstruct.place(renderString(text), a);
			return a;
		},

		sortComparator: function(item){
			return item;
		}
	});

	var IndigoLinkList = declare(LinkList, {
		constructor: function(items, linkPrefix){
			this.linkPrefix = linkPrefix;
		},

		renderListItem: function(li, guid){
			var href = this.linkPrefix + guid;
			var a = this.renderLink(href, guid);
			domConstruct.place(a, li);
		}
	});

	var FileList = declare(LinkList, {
		constructor: function(items, bundle){
			this.bundle = bundle;
		},

		renderListItem: function(li, file){
			if(file.local){
				var span = domConstruct.create('span', { 'class': 'local' }, li);
				domConstruct.place(renderString(file.name + " (syncing\u2026)"), span);
			} else {
				var timestamp = this.bundle.timestamp;
				var formattedTimestamp = (this.bundle.endpoint == 'acid' || timestamp != ~~timestamp)
					? timestamp.toFixed(6)
					: timestamp;

				/* MAJOR HAX
				 * Herder doesn't decode the email address, so the directory on disk is literally "Swapnil%20Tayade"
				 * instead of "Swapnil Tayade". I am giving Herder what it wants only for alacrity, not purity
				 */
				var flumeCompatibleEmail = (this.bundle.email && this.bundle.email.replace(/ /g, '%2520'));
				var href = [FileList.flumePrefix, this.bundle.endpoint, flumeCompatibleEmail, formattedTimestamp, file.name].join('/');

				var a = this.renderLink(href, file.name);
				domConstruct.place(a, li);
			}
		},

		sortComparator: function(file){
			//sort local files last, then nested sort by name ascending
			return (file.local ? '1' : '0') + file.name;
		}
	});

	FileList.flumePrefix = "http://10.100.0.32/denimlogs/uploaded-logs";

	return DetailsPane;

});