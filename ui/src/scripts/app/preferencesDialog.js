define([
	'lodash/lodash',
	'dijit/registry',
	'dijit/Dialog',
	'dijit/form/Textarea',
	'dijit/form/Button',
	'dijit/form/Form',
	'dojo/on',
	'dojo/request',
	'dojo/dom',
	'dojo/query',
	'dojox/form/ListInput',
	'dojo/keys',
	'dojo/dom-attr'
],
function(
	_,
	registry,
	Dialog,
	Textarea,
	Button,
	Form,
	on,
	request,
	dom,
	query,
	ListInput,
	keys,
	domAttr
){

	var preferencesDialog = function(){
		_.bindAll(this);
	};

	preferencesDialog.prototype.init = _.once(function(){
		this.widget     = registry.byId('preferencesDialog');
		this.form       = registry.byId('uploadNotificationsForm');
		this.capsLock   = registry.byId('capsLock');
		this.recipients = registry.byId('recipients');

		this.uploadNotificationsVersion = null;

		on(this.widget, 'submit', this.onSubmit);

		query(".cancel", this.form.domNode).on("click", _.bind(function(event){
			event.preventDefault();
			this.widget.hide();
		}, this));

		/**
		 * Don't submit the form when the user presses enter to finish adding an email
		 */
		query(".dojoxListInputNode", this.recipients.domNode).on('keydown', function(event){
			if(event.keyCode == keys.ENTER){
				event.preventDefault();
			}
		});
	});

	preferencesDialog.prototype.populate = function(){
		return request('/preferences/uploadNotifications', { handleAs: 'json' }).then(_.bind(function(data){
			this.recipients.set('value', data.recipients);
			this.capsLock.set('checked', data.capsLock);
			this.uploadNotificationsVersion = data._version;
		}, this));
	};

	preferencesDialog.prototype.onSubmit = function(event){
		event.preventDefault();
		var data = {};
		data._version = this.uploadNotificationsVersion;
		data.recipients = this.recipients.get('value');
		data.capsLock = this.capsLock.get('checked');

		registry.byId('save').set('disabled', true);

		request('/preferences/uploadNotifications', {
			method: 'PUT',
			data: JSON.stringify(data),
			headers: { 'Content-Type': 'application/json' }
		}).then(
			_.bind(function(){
				this.widget.hide();
			}, this),
			function(err){
				window.alert(err);
			});
	};

	preferencesDialog.prototype.show = function(){
		//TODO show loading spinner i guess
		this.init();

		this.populate()
			.then(_.bind(function(){
				registry.byId('save').set('disabled', false);
				this.widget.show();
			}, this))
			.then(function(){}, function(err){
				console.error(err);
			});
	};

	return new preferencesDialog();
});