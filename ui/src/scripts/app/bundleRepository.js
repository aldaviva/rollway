define([
	'require',
	'dojo/store/JsonRest'
], function(
	require,
	JsonRest)
{

	var jsonRest = new JsonRest({
		target: 'bundles',
		sortParam: 'sort'
	});

	return jsonRest;
});