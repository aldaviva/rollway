define([
	'lodash/lodash',
	'dojo/_base/declare',
	'dojo/dom-construct',
	'dijit/_WidgetBase',
	'dijit/form/TextBox',
	'dijit/form/DateTextBox',
	'dojo/router',
	'dojo/io-query',
	'dojo/hash'
], function(
	_,
	declare,
	domConstruct,
	_WidgetBase,
	TextBox,
	DateTextBox,
	router,
	ioQuery,
	hash)
{

	return declare([_WidgetBase], {

		"class": "FilterPane", 

		query: {},
		gridPane: null,

		constructor: function(){
		},

		buildRendering: function(){
			this.inherited(arguments);
			var el = this.domNode;

			domConstruct.create('h2', { innerHTML: 'Search' }, el);

			var content = domConstruct.create('div', { 'class': 'content' }, el);

			var emailLabel = domConstruct.create('div', { 'class': 'label' }, content);
			domConstruct.create('h3', { innerHTML: 'Email' }, emailLabel);
			
			this.emailField = new TextBox({
				value: "",
				placeholder: "any email",
				intermediateChanges: true
			});

			this.emailField
				.placeAt(emailLabel)
				.on("change", _.debounce(_.bind(function(newValue){
					this.updateQuery('email', newValue || null);
				}, this), 150));

			var dateLabel = domConstruct.create('div', { 'class': 'label' }, content);
			domConstruct.create('h3', { innerHTML: 'Date' }, dateLabel);
			
			this.dateField = new DateTextBox({
				placeholder: "any date"
			});
			
			this.dateField.placeAt(dateLabel)
				.on("change", _.bind(function(newValue){
					this.updateQuery('timestamp', newValue
						? newValue.getTime()/1000
						: null);
				}, this));
		},

		updateQuery: function(key, value){
			var hashQueryObject = ioQuery.queryToObject(hash());

			if(value !== null){
				this.query[key] = value;
				hashQueryObject[key] = value;
			} else {
				delete this.query[key];
				delete hashQueryObject[key];
			}

			this.gridPane.grid.setQuery(this.query);

			hash(ioQuery.objectToQuery(hashQueryObject));
		},

		postCreate: function(){
			this.onHashChange(hash());
		},

		onHashChange: function(newHash){
			var hashQueryObject = ioQuery.queryToObject(newHash);

			if((hashQueryObject.email||"") != this.emailField.get('value')){
				if(hashQueryObject.email !== undefined){
					this.emailField.set('value', hashQueryObject.email);
				} else {
					this.emailField.set('value', '');
				}
			}

			var hashDate = new Date(hashQueryObject.timestamp*1000);
			if(hashDate != this.dateField.get('value')){
				if(hashQueryObject.timestamp !== undefined){
					this.dateField.set('value', hashDate);
				} else {
					this.dateField.set('value', null);
				}
			}
		}

	});

});