define(function(require){
	var _                  = require('lodash');
	var accounting         = require('accounting');
	var analyticsMapReduce = require('analyticsMapReduce');

	var period = (window.location.search.replace('?', '') || 'day');
	var graphs = {};
	var storedReports = {};

	var plurals = {
		'time'            : 'times',
		'file'            : 'files',
		'kilobyte'        : 'kilobytes',
		'megabyte'        : 'megabytes',
		'second'          : 'seconds',
		'megabyte/second' : 'megabytes/second'
	};

	var reportNumberCoefficients = {
		'event:server:bytesProcessed': 1/1024/1024, //convert bytes to megabytes
		'event:server:scanTime': 1/1000, //convert milliseconds to seconds
		'event:server:scanVelocity': 1000/1024/1024 //convert bytes/millisecond to megabytes/second
	};

	function init(){
		$('nav .'+period).addClass('current');
		initGraphs();
		downloadReports();
		initEventStream();
		startGraphScroll();
	}

	function downloadReports(){
		$.get("analytics/reports/"+period, function(res){
			onReportsDownloaded(res);
		});
	}

	function onReportsDownloaded(reports){
		_.each(reports, function(report){
			var reportName = report._id;

			storedReports[reportName] = report.value;

			if(period == reportName.match(/_([^_]+?)$/)[1]){

				var topic = reportName.split('_')[0];
				renderGraph(topic);

				if(topic == 'event:server:scanVelocity'){
					updateAverage(topic, false, report.value.totalFileSizeBytes, report.value.totalDurationMillis);
				} else {
					updateCount(topic, false, report.value.count);
				}
			}
		});
	}

	function updateCount(topic, isDelta, value){
		var pane     = $('#'+topic.replace(/:/g, '-'));

		var newValue = updateNumber(pane, topic, function(oldValue){
			return value + (isDelta ? oldValue : 0);
		}, function(newValue, coefficient, places){
			return accounting.formatNumber(newValue * coefficient, places);
		});

		updateUnits(pane, newValue);
	}

	function updateAverage(topic, isDelta, numeratorValue, denominatorValue){
		var pane           = $('#'+topic.replace(/:/g, '-'));
		
		var newValue = updateNumber(pane, topic, function(oldValue){
			var newNumerator   = numeratorValue + (isDelta ? (oldValue.numerator || 0) : 0);
			var newDenominator = denominatorValue + (isDelta ? (oldValue.denominator || 0) : 0);
			return { numerator: newNumerator, denominator: newDenominator };
		}, function(newValue, coefficient, places){
			return accounting.formatNumber((newValue.numerator / newValue.denominator) * coefficient, places);
		});
		updateUnits(pane, newValue);
	}

	function updateNumber(pane, topic, newValueGenerator, formatter){
		var el       = $('.number', pane);
		var oldValue = el.data('value');
		var newValue = newValueGenerator(oldValue);
		el.data('value', newValue);

		var coefficient = reportNumberCoefficients[topic] || 1;
		var places = el.data('places');
		var formattedValue = formatter(newValue, coefficient, places);

		el.text(formattedValue);

		return newValue;
	}

	function updateUnits(pane, newValue){
		var unitsEl = $('.units', pane);
		var singularLabel = unitsEl.data('singular');
		var unitsLabel = (newValue == 1)
			? singularLabel
			: plurals[singularLabel];
		unitsEl.text(unitsLabel);
	}

	function initGraphs(){
		graphs['event:client:pageLoaded']     = d3.select('#event-client-pageLoaded .graph');
		graphs['event:client:fileViewed']     = d3.select('#event-client-fileViewed .graph');
		graphs['event:client:indigoLinked']   = d3.select('#event-client-indigoLinked .graph');
		graphs['event:server:fileProcessed']  = d3.select('#event-server-fileProcessed .graph');
		graphs['event:server:bytesProcessed'] = d3.select('#event-server-bytesProcessed .graph');
		graphs['event:server:scanTime']       = d3.select('#event-server-scanTime .graph');
		graphs['event:server:scanVelocity']   = d3.select('#event-server-scanVelocity .graph');
	}

	function renderGraph(topic){
		if(graphs[topic]){
			var report = storedReports[topic + '_' + period];

			var isScanVelocity = (topic == 'event:server:scanVelocity');

			var yMax = 0;
			var series = _.map(report.series, function(val, key){
				if(isScanVelocity){
					return {key: key, value: (val.durationMillis == 0 ? 0 : val.fileSizeBytes/val.durationMillis)};
				} else {
					return {key: key, value: val};
				}
			});
			series = _.sortBy(series, function(item){
				return item.key;
			});

			series = series.slice(-1 * analyticsMapReduce.increments[period].incrCount);
			
			_.each(series, function(datum){
				yMax = Math.max(yMax, datum.value);
			});

			var yScale = d3.scale.linear()
				.range(["0%", "100%"])
				.domain([0, yMax]);

			var graph = graphs[topic].selectAll('div.bar').data(series);

			graph.enter()
				.append('div')
					.attr('class', 'bar')
					.attr('data-timestamp', function(d){ return d.key; })
					.attr('data-value', function(d){ return d.value; })
					.style("height", function(d){ return yScale(d.value); })
					.style("width", function(){ return 100/report.incrementCount + '%'; })
					.append('div')
						.attr('class', 'shadow');

			graph.transition()
				.duration(0)
				.attr('data-timestamp', function(d){ return d.key; })
				.attr('data-value', function(d){ return d.value; })
				.style("height", function(d){ return yScale(d.value); });

			graph.exit().remove();
		}
	}

	function onIncrementEvent(topic, data){
		var reportName = topic + '_' + period;
		var report     = storedReports[reportName];
		var timestamp  = data.timestamp;

		var incrementSize      = analyticsMapReduce.increments[period].incrSize;
		var quantizedTimestamp = analyticsMapReduce.quantizeTimestamp(timestamp, incrementSize);

		var delta = 1;

		report.series[quantizedTimestamp] = delta + (report.series[quantizedTimestamp] || 0);

		renderGraph(topic);
		updateCount(topic, true, delta);
	}

	function onScanVelocityEvent(data){
		var deltaDurationMillis = data.durationMillis;
		var deltaFileSizeBytes = data.fileSizeBytes;

		var incrementSize      = analyticsMapReduce.increments[period].incrSize;
		var quantizedTimestamp = analyticsMapReduce.quantizeTimestamp(data.timestamp, incrementSize);

		var fileProcessedSeries = storedReports['event:server:fileProcessed_'+period].series;
		fileProcessedSeries[quantizedTimestamp] = 1 + (fileProcessedSeries[quantizedTimestamp] || 0);

		var bytesProcessedSeries = storedReports['event:server:bytesProcessed_'+period].series;
		bytesProcessedSeries[quantizedTimestamp] = deltaFileSizeBytes + (bytesProcessedSeries[quantizedTimestamp] || 0);

		var scanTimeSeries = storedReports['event:server:scanTime_'+period].series;
		scanTimeSeries[quantizedTimestamp] = deltaDurationMillis + (bytesProcessedSeries[quantizedTimestamp] || 0);

		var scanVelocitySeries = storedReports['event:server:scanVelocity_'+period].series;
		scanVelocitySeries[quantizedTimestamp].fileSizeBytes = deltaFileSizeBytes + (scanVelocitySeries[quantizedTimestamp].fileSizeBytes || 0);
		scanVelocitySeries[quantizedTimestamp].durationMillis = deltaDurationMillis + (scanVelocitySeries[quantizedTimestamp].durationMillis || 0);

		renderGraph('event:server:fileProcessed');
		renderGraph('event:server:bytesProcessed');
		renderGraph('event:server:scanTime');
		renderGraph('event:server:scanVelocity');

		updateCount('event:server:fileProcessed', true, 1);
		updateCount('event:server:bytesProcessed', true, deltaFileSizeBytes);
		updateCount('event:server:scanTime', true, deltaDurationMillis);
		updateAverage('event:server:scanVelocity', true, deltaFileSizeBytes, deltaDurationMillis);
	}

	function initEventStream(){
		var socket = io.connect(window.location.protocol + '//' + window.location.host + '/analytics');
		console.log('connecting...');

		socket.on('connect', function(){
			console.log('listening...');
		});

		_([
			'event:client:pageLoaded',
			'event:client:indigoLinked',
			'event:client:fileViewed',
		]).each(function(topic){
			socket.on(topic, function(data){
				console.log(topic, data);
				onIncrementEvent(topic, data);
			});
		});

		socket.on('event:server:scanVelocity', function(data){
			console.log(data);
			onScanVelocityEvent(data);
		});

		socket.on("analytics:reportsGenerated", function(data){
			console.log("new reports available, fetching...");
			downloadReports();
		});

		socket.on('disconnect', function(){
			console.log('disconnected');
		});

		socket.on('connect_failed', function (data) {
			console.log(data || 'connect_failed');
		});
	}

	function startGraphScroll(){
		window.setInterval(function(){
			var now = new Date().getTime();
			var increment = analyticsMapReduce.increments[period];
			var mostRecentQuantizedTimestamp = analyticsMapReduce.quantizeTimestamp(now, increment.incrSize);

			_.each(storedReports, function(storedReport, reportName){

				var topic = reportName.split('_')[0];
				var isScanVelocity = (topic == 'event:server:scanVelocity');

				for(var buckets = 0, timestamp = mostRecentQuantizedTimestamp; buckets < increment.incrCount; buckets++, timestamp -= increment.incrSize){
					if(typeof storedReport.series[timestamp] == 'undefined'){
						if(isScanVelocity){
							storedReport.series[timestamp] = {
								fileSizeBytes: 0,
								durationMillis: 0
							};
						} else {
							storedReport.series[timestamp] = 0;
						}
					}
				}

				renderGraph(topic);
			});
		}, analyticsMapReduce.increments[period].incrSize / 2);
	}

	init();
});