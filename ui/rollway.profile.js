var profile = (function(){

	return {
		basePath: "src/scripts",
		releaseDir: "../../target/scripts",
		action: "release",

		packages:[{
			name     : "app",
			location : "app"
		},{
			name     : "dojo",
			location : "lib/dojo/dojo"
		},{
			name     : "dijit",
			location : "lib/dojo/dijit"
		},{
			name     : "dojox",
			location : "lib/dojo/dojox"
		},{
			name     : "lodash",
			location : "../../../node_modules/lodash/dist"
		}],

		layers           : {
			"dojo/dojo": {
				include    : ['dojo/dojo', 'app/index'],
				customBase : true,
				boot       : true
			}
		},
		optimize         : '',
		layerOptimize    : '', //closure, shrinksafe, ''. skip this because we minify with uglify-js later in the build process
		selectorEngine   : 'lite',
		mini             : true,
		staticHasFeatures: {
			'dojo-amd-factory-scan' : 0,
			'dojo-combo-api'        : 0,
			'dojo-dom-ready-api'    : 0,
			'dojo-firebug'          : 0,
			'dojo-log-api'          : 0,
			'dojo-modulePaths'      : 0,
			'dojo-moduleUrl'        : 0,
			'dojo-publish-privates' : 0,
			'dojo-sync-loader'      : 0,
			'dojo-test-sniff'       : 0,
			'dojo-trace-api'        : 0,
			'dojo-timeout-api'      : 0,
			'dojo-undef-api'        : 0,
			'dojo-xhr-factory'      : 0
		}
	};
})();