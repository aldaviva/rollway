({
	baseUrl: "src/analytics/scripts",
	include: [ 'almond', /*'socket.io',*/ 'zepto', 'd3', 'main' ],
	paths: {
		'accounting'         : '../../../../node_modules/accounting/accounting',
		'almond'             : '../../../../node_modules/almond/almond',
		'd3'                 : '../../../../node_modules/d3/d3',
		'lodash'             : '../../../../node_modules/lodash/lodash',
		'analyticsMapReduce' : '../../../../lib/analyticsMapReduce',
		// 'socket.io'       : '../../../../node_modules/socket.io/node_modules/socket.io-client/dist/socket.io',
		'zepto'              : '../../scripts/lib/zepto/zepto'
	},
	out: "target/analytics/scripts/all.js",
	optimize: 'none',
	skipModuleInsertion: true,
	onBuildWrite: function(moduleName, path_, contents){
		return "\n\n/******** "+moduleName+" start ********/\n\n"+contents+"\n\n/******** "+moduleName+" end ********/";
	},
	wrap: {
		start: "(function(){",
		end: "\n\nrequire('main');\n})();"
	}
})