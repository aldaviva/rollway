DOJO_VERSION := 1.8.3

JSCOVERAGE := jscoverage
JSL := jsl
JSON := json
LESSC := node_modules/less/bin/lessc
MOCHA := node_modules/mocha/bin/mocha
NODE := node
NPM := npm
RESTDOWN := restdown
REQUIREJS := node_modules/requirejs/bin/r.js
TESTAMENT := node /home/ben/testament
UGLIFY := node_modules/uglify-js/bin/uglifyjs

TEST_REPORTER := nyan

PACKAGE_VERSION := `$(JSON) -f package.json version`
PACKAGE_FILENAME := target/rollway-$(PACKAGE_VERSION).tgz

BUILDONWRITE_SERVER := http://10.4.4.251:10990

all: lint ui test doc

ui: lint js css images html buildonwrite

package: lint js minify css images html test doc
	@mkdir -p target
	@echo "Packaging..."
	@tar --exclude=./node_modules/less --exclude=./node_modules/mocha --exclude=./node_modules/rewire --exclude=./node_modules/uglify-js --exclude=./node_modules/sinon --exclude=./node_modules/sinon-mocha --exclude=./node_modules/accounting --exclude==./node_modules/almond --exclude=./node_modules/d3 --exclude=./node_modules/requirejs -czf $(PACKAGE_FILENAME) LICENSE config.json.example doc index.js lib Makefile ./node_modules package.json Readme.md rollway.sh ui/target
	@echo "Packaged $(PACKAGE_FILENAME)"

js:
	@$(REQUIREJS) -o ui/analytics.profile.js
	@ui/src/scripts/lib/dojo/util/buildscripts/build.sh --profile ui/rollway.profile.js
	@cp ui/target/scripts/dojo/dojo.js ui/target/scripts/all.js

minify:
	@echo "Minifying..."
	@$(UGLIFY) ui/target/scripts/dojo/dojo.js --output ui/target/scripts/all.js --mangle --compress warnings=false
	@$(UGLIFY) ui/target/analytics/scripts/all.js --output ui/target/analytics/scripts/all.js --mangle --compress warnings=false

css:
	@mkdir -p ui/target/styles ui/target/analytics/styles
	@$(LESSC) ui/src/styles/all.less ui/target/styles/all.css
	@$(LESSC) ui/src/analytics/styles/all.less ui/target/analytics/styles/all.css
	@mkdir -p ui/target/fonts ui/target/analytics/fonts
	@cp -r ui/src/fonts/* ui/target/fonts/
	@cp -r ui/src/analytics/fonts/* ui/target/analytics/fonts

images:
	@-mkdir -p ui/target/images ui/target/analytics/images
	@cp -r ui/src/images/* ui/target/images/
	@cp -r ui/src/analytics/images/* ui/target/analytics/images/
	@cp ui/src/favicon.ico ui/target/favicon.ico

html:
	@cp ui/src/*.html ui/target/
	@cp ui/src/analytics/*.html ui/target/analytics
	@sed -i "s:{{VERSION}}:$(PACKAGE_VERSION):" ui/target/index.html
	@sed -i "s:-SNAPSHOT:-SNAPSHOT-`date +%s`:" ui/target/index.html

.PHONY: buildonwrite
buildonwrite:
	@#sed -i 's^</body>^	<script src="$(BUILDONWRITE_SERVER)/" type="text/javascript"></script>\n</body>^' ui/target/index.html

.PHONY: doc
doc:
	@$(RESTDOWN) -q -b doc doc/index.restdown

.PHONY: lint
lint:
	@$(JSL) --conf=build/jsl.conf --nofilelisting --nologo --nosummary *.js lib/*.js ui/src/scripts/app/*.js

test-all: test test-client

.PHONY: test
test: lint
	@if [ -d ".pid" ]; then cat .pid | xargs kill -INT; fi; exit 0

	@rm -rf test/tempData .pid
	@$(NODE) $(MOCHA) --reporter $(TEST_REPORTER) --bail --slow 1000 test/server
	@rm -rf test/tempData .pid

.PHONY: test-client
test-client: lint html
	@if [ -d ".pid" ]; then cat .pid | xargs kill -INT; fi; exit 0

	$(eval TESTAMENT_JOB := $(shell $(TESTAMENT) generate-job))
	$(eval TESTAMENT_URL := $(shell $(TESTAMENT) get-script-url --job $(TESTAMENT_JOB)))
	@sed -i 's^</body>^	<script src="$(TESTAMENT_URL)" type="text/javascript"></script>\n	</body>^' ui/target/index.html

	@$(NODE) . --config test/testConfig.json &
	@$(TESTAMENT) test --job $(TESTAMENT_JOB) --config test/testament.config.json

	@# leaving the server running is helpful for debugging tests. remember to kill the server before re-running the tests

.PHONY: coverage
coverage:
	@rm -rf lib-cov coverage.html
	@$(JSCOVERAGE) lib lib-cov
	@ROLLWAY_COVERAGE=1 $(MOCHA) --reporter html-cov test/server > test/coverage.html
	@rm -rf lib-cov test/tempData
	@echo "Done. See test/coverage.html for code coverage report."

clean:
	@rm -rf ui/target test/tempData lib-cov coverage.html target

clean-deps:
	@rm -rf node_modules/* ui/src/scripts/lib/*

run:
	@$(NODE) .

install:
	@cp rollway.sh /etc/init.d/rollway
	@sed -i 's:PROGRAM_DIR=.*$$:PROGRAM_DIR=$(CURDIR):' /etc/init.d/rollway
	@update-rc.d rollway defaults
	@echo "Installed. Use /etc/init.d/rollway {start|stop|restart|status}"

deps: dojo zepto
	@$(NPM) install

dojo: ui/src/scripts/lib/dojo/dojo
zepto: ui/src/scripts/lib/zepto/zepto.js

ui/src/scripts/lib/zepto/zepto.js:
	@-mkdir -p ui/src/scripts/lib/zepto/
	@wget -O ui/src/scripts/lib/zepto/zepto.js http://zeptojs.com/zepto.js

ui/src/scripts/lib/dojo/dojo:
	@rm -rf ui/src/scripts/lib/dojo
	@wget -O ui/src/scripts/lib/dojo.tar.gz http://download.dojotoolkit.org/release-$(DOJO_VERSION)/dojo-release-$(DOJO_VERSION)-src.tar.gz
	@tar xzf ui/src/scripts/lib/dojo.tar.gz -C ui/src/scripts/lib/
	@mv ui/src/scripts/lib/dojo-release-$(DOJO_VERSION)-src/ ui/src/scripts/lib/dojo
	@rm ui/src/scripts/lib/dojo.tar.gz