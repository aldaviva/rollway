(function(){
	var db;
	var child;

	module.exports.start = function(done){
		var child_process = require('child_process');
		var mongo = require('mongodb');
		var config = require('../../testConfig');

		db = new mongo.Db(config.dbName, new mongo.Server(config.dbHost, config.dbPort), {journal: true });

		prepareDatabaseContents(db, done);

		// startChild(child);
	};

	module.exports.stop = function(done){
		/*db.collection('bundles', function(err, bundlesCollection){
			bundlesCollection.remove({}, function(err){
				if(err != null) throw err;
				db.close(done);
			});
		});*/
		db.close(done);
	};

	function startChild(child){

	}

	function stopChild(child){
		child.kill('SIGINT');
	}

	function prepareDatabaseContents(db, done){
		var now = new Date();
		var documents = [];

		documents.push({
			_id: generateGuid(),
			timestamp: now.getTime()/1000,
			email: 'ben@bluejeans.com',
			comment: 'created now',
			endpoint: 'Browser',
			meetings: [generateGuid(), generateGuid()],
			legs: [],
			files: ['rbjnplugin.1234567890_1234']
		});

		documents.push({
			_id: generateGuid(),
			timestamp: new Date(now.getFullYear(), now.getMonth(), now.getDate() - 2).getTime()/1000,
			email: 'ben@aldaviva.com',
			comment: 'created two days in the past',
			endpoint: 'acid',
			meetings: [],
			legs: [generateGuid()],
			files: ['call.log']
		});

		db.open(function(){
			db.collection('bundles', function(err, bundlesCollection){
				bundlesCollection.remove({}, function(err){
					if(err != null) throw err;
					bundlesCollection.insert(documents, function(err, results){
						if(err != null) throw err;
						module.exports.logger.info("Database %s contains mock data.", db.databaseName);
						done();
					});
				});
			});
		});
	}

	function randString(a,b,c){for(a=a||32;a--;c=[c]+(0|Math.random(b=b||16)*b).toString(b));return c};

	function generateGuid(){
		return [8,4,4,4,12].map(function(length){return randString(length); }).join('-');
	}

})();