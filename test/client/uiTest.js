mocha.globals(['1']);  //dojo.on adds this to patch IE DOM event emissions

describe("searching", function(){
	var on = window.require('dojo/on');

	var networkDelay = 1000;
	var renderDelay = 600;

	describe("by email", function(){
		var emailField, emailAddress;

		before(function(done){
			emailField = dojo.byId('dijit_form_TextBox_0');
			emailAddress = "@bluejeans";

			emailField.value = emailAddress;

			/* This is how you get Dojo to run the change handlers for the text widget */
			on.emit(emailField, 'paste', { bubbles: true, cancelable: true }); //wtf firefox

			setTimeout(done, networkDelay);
		});

		it("updates the URL hash", function(){
			/* Using href instead of hash because Chrome decodeURL()'s the hash, and Firefox doesn't, but they both leave href alone */
			var urlHash = window.location.href;

			try {
				expect(urlHash).to.include('email='+encodeURIComponent(emailAddress));
			} catch(e){
				alert('hash missing email query');
			}
		});

		it("filters visible grid rows", function(){
			var rows = dojo.query('.dojoxGridView .dojoxGridCell[idx="0"]')
			expect(rows).to.have.length(1);

			rows.map(function(node){
					return node.firstChild.nodeValue;
				}).forEach(function(address){
					expect(address).to.contain(emailAddress);
				});
		});

		after(function(done){
			emailField.focus();
			emailField.value = "";
			emailField.blur();
			setTimeout(done, renderDelay);
		});
	});

	describe("by date", function(){
		var now = new Date();
		var dateField;
		var dateDropDownArrow;

		before(function(done){
			dateField = dojo.byId('dijit_form_DateTextBox_0');
			dateDropDownArrow = dojo.query('.dijitArrowButtonInner')[0];

			on.emit(dateDropDownArrow, "mousedown", { bubbles: true, cancelable: true });
			setTimeout(done, renderDelay);
		});

		before(function(done){
			var todayCalendarCell = dojo.query('.dijitCalendarCurrentDate')[0];
			on.emit(todayCalendarCell, "click", { bubbles: true, cancelable: true });
			setTimeout(done, networkDelay);
		});

		it("updates the URL hash", function(){
			var urlHash = window.location.href;

			var expectedDatestamp = Math.floor(new Date(now.getFullYear(), now.getMonth(), now.getDate()).getTime()/1000);

			expect(urlHash).to.contain('timestamp='+expectedDatestamp);
		});

		it("filters visible grid rows", function(){
			var expectedDateString = [now.getMonth()+1, now.getDate(), now.getFullYear() - 2000].join('/');
			var rows = dojo.query('.dojoxGridView .dojoxGridCell[idx="1"]');
			expect(rows).to.have.length(1);
			
			rows.map(function(node){
					return node.firstChild.nodeValue;
				}).forEach(function(address){
					expect(address).to.contain(expectedDateString);
				});
		});

		after(function(done){
			dateField.focus();
			dateField.value = '';
			dateField.blur();
			setTimeout(done, networkDelay);
		});
	});
});