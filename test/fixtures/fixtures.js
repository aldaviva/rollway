var _     = require('underscore');
var mongo = require('mongodb');

var config = {
	dbName              : 'rollway',
	dbHost              : '127.0.0.1',
	dbPort              : 27017,
	recordToInsertCount : 1024
};

/**
 * @param a : length of string (default = 32 characters)
 * @param b : base (default = 16 = 0-f)
 * @returns string of length a filled with random characters in base b
 */
function randString(a,b,c){for(a=a||32;a--;c=[c]+(0|Math.random(b=b||16)*b).toString(b));return c};

/**
 * @returns a timestamp randomly distributed between now and one year ago
 */
function generateTimestamp(){
	return 0|new Date().getTime()/1000 - _.random(60 * 60 * 24 * 365);
}

function generateEmail(){
	return randString(12)+'@bluejeans.com';
}

function generateGuid(){
	return _([8,4,4,4,12]).map(function(length){return randString(length); }).join('-');
}

function generateBundle(){
	return {
		_id       : generateGuid(),
		fixture   : true,
		timestamp : generateTimestamp(),
		email     : generateEmail(),
		comment   : randString(100, 36),
		endpoint  : _.random(1) ? 'Browser' : 'acid',
		meetings  : _(4).chain().random().range().map(generateGuid).value(),
		legs      : _(4).chain().random().range().map(generateGuid).value(),
		files     : _(4).chain().random().range().map(generateGuid).map(function(guid){ return {
			name    : 'rbjnplugin.'+generateTimestamp()+'_'+randString(4,9)+'.log',
			local   : !!_.random(1),
			scanned : !!_.random(1)
		} }).value(),
	};
}

var db = new mongo.Db(config.dbName, new mongo.Server(config.dbHost, config.dbPort), {
	journal: true
});

db.open(function(){
	db.collection('bundles', function(err, bundlesCollection){
		bundlesCollection.remove({ fixture: true}, function(err){
			var bundlesToInsert = _(config.recordToInsertCount).chain().range().map(generateBundle).value();

			bundlesCollection.insert(bundlesToInsert, function(err, results){
				bundlesCollection.count(function(err, totalCount){
					console.log('Inserted %d fixture records into %s (%d total records)', results.length, config.dbName, totalCount);
					db.close();
				});
			});
		});
	});
});