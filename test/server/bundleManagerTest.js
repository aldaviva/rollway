var testConfig    = require('../testConfig');
var config        = require(process.env.ROLLWAY_COVERAGE ? '../../lib-cov/config' : '../../lib/config').init(testConfig);
var assert        = require('assert');
var bundleManager = require(process.env.ROLLWAY_COVERAGE ? '../../lib-cov/bundleManager' : '../../lib/bundleManager');

describe('BundleManager', function(){
	describe('.getPathInfo()', function(){
		var getPathInfo = bundleManager.getPathInfo;

		it("parses relative paths with email address and no filename", function(){
			var pathInfo = getPathInfo("Browser/foo@bar.com/1234567890");
			assert.equal(pathInfo.endpoint, "Browser");
			assert.equal(pathInfo.email, "foo@bar.com");
			assert.equal(pathInfo.timestamp, 1234567890);
		});

		it("parses relative paths with email address and filename", function(){
			var pathInfo = getPathInfo("Browser/foo@bar.com/1234567890/rbjnplugin.123456");
			assert.equal(pathInfo.endpoint, "Browser");
			assert.equal(pathInfo.email, "foo@bar.com");
			assert.equal(pathInfo.timestamp, 1234567890);
		});

		it("parses relative paths with no email address and no filename", function(){
			var pathInfo = getPathInfo("Browser/1234567890");
			assert.equal(pathInfo.endpoint, "Browser");
			assert.equal(pathInfo.email, undefined);
			assert.equal(pathInfo.timestamp, 1234567890);
		});
		
		it("parses relative paths with no email address and filename", function(){
			var pathInfo = getPathInfo("Browser/1234567890/rbjnplugin.123456");
			assert.equal(pathInfo.endpoint, "Browser");
			assert.equal(pathInfo.email, undefined);
			assert.equal(pathInfo.timestamp, 1234567890);
		});

		it("parses relative paths with no email address, filename, and repeating slashes", function(){
			var pathInfo = getPathInfo("Browser//1234567890/rbjnplugin.123456");
			assert.equal(pathInfo.endpoint, "Browser");
			assert.equal(pathInfo.email, undefined);
			assert.equal(pathInfo.timestamp, 1234567890);
		});

		/*
		 * We don't parse absolute URLs anywhere.
		 * Allowing the email portion of the path to be a full name makes this form of input ambiguously parsable.
		 */
		// it("parses absolute paths with email address and filename", function(){
		// 	var pathInfo = getPathInfo("/home/denimuser/logs/Browser/foo@bar.com/1234567890/rbjnplugin.123456");
		// 	assert.equal(pathInfo.endpoint, "Browser");
		// 	assert.equal(pathInfo.email, "foo@bar.com");
		// 	assert.equal(pathInfo.timestamp, 1234567890);
		// });

		it("parses timestamps that are floating point numbers", function(){
			var pathInfo = getPathInfo("acid/Firstname Lastname/1234567890.123456/rbjnplugin.123456");
			assert.equal(pathInfo.endpoint, "acid");
			assert.equal(pathInfo.email, "Firstname Lastname");
			assert.equal(pathInfo.timestamp, 1234567890.123456);
		});

		it("parses relative paths with a full name instead of an email address, and no filename", function(){
			var pathInfo = getPathInfo("Browser/Foo Bar/1234567890");
			assert.equal(pathInfo.endpoint, "Browser");
			assert.equal(pathInfo.email, "Foo Bar");
			assert.equal(pathInfo.timestamp, 1234567890);
		});

		it("parses relative paths with a full name instead of an email address, and a filename", function(){
			var pathInfo = getPathInfo("Browser/Foo B Bar/1234567890/rbjnplugin.123456");
			assert.equal(pathInfo.endpoint, "Browser");
			assert.equal(pathInfo.email, "Foo B Bar");
			assert.equal(pathInfo.timestamp, 1234567890);
		});

		it("parses relative paths with email address and filename with leading period", function(){
			var pathInfo = getPathInfo("Browser/fbmerge_prasad@gmail.com/1362630732/.gbjnplugin.1362417203_3416.log");
			assert.equal(pathInfo.endpoint, "Browser");
			assert.equal(pathInfo.email, "fbmerge_prasad@gmail.com");
			assert.equal(pathInfo.timestamp, 1362630732);
		});
	});
});