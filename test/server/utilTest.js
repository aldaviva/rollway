var assert        = require('assert');
var util = process.env.ROLLWAY_COVERAGE ? '../../lib-cov/util' : '../../lib/util';

describe('util', function(){
	describe('Boolean.parseBoolean', function(){
		it('parses true values', function(){
			var inputs = ["true", "True", "TRUE", "yes", "1"];
			inputs.forEach(function(input){
				assert(Boolean.parseBoolean(input), "expected Boolean.parseBoolean("+input+") = true");
			});
		});

		it('parses false values', function(){
			var inputs = ["false", "False", "FALSE", "no", "0", null, undefined];
			inputs.forEach(function(input){
				assert(!Boolean.parseBoolean(input), "expected Boolean.parseBoolean("+input+") = false");
			});
		});
	});
});