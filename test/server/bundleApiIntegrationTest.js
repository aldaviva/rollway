var fs           = require('fs');
var path         = require('path');
var mongo        = require('mongodb');
var testConfig   = require('../testConfig');
var config       = require(process.env.ROLLWAY_COVERAGE ? '../../lib-cov/config' : '../../lib/config').init(testConfig);
var assert       = require('assert');
var rollway      = require('../..');
var http         = require('http');

var db;
var bundles;

// Keep-Alive socket limits in Node 0.10.0
// https://github.com/joyent/node/issues/4769
http.globalAgent.maxSockets = 65535;

describe('Bundle API', function(){
	var filename = 'acid/ben@aldaviva.com/1360114618.123450/rbjnplugin.1359440216_3864.log';
	var catchmark = 'a482e3fc-f49d-0d4e-d561-ae69d691ea1e';
	// var catchmark = '836b2316-390f-e55f-1405-0748c95fa645'; //integer timestamp

	before(function(done){
		rollway.startedPromise
			.then(function(){
				db = new mongo.Db(config.dbName, new mongo.Server(config.dbHost, config.dbPort), {
					journal: true
				});
				db.open(function(){
					db.collection('bundles', function(err, bundles_){
						bundles = bundles_;
						bundles.remove({}, function(err, recordsRemoved){
							done();
						});
					});
				});
			});
	});

	describe('when empty', function(){
		it('returns an empty array', function(done){
			http.request({ method: 'GET', path: '/bundles', port: config.apiPort }, function(res){
				var body = '';
				res.on('data', function(chunk){
					body += chunk;
				});
				res.on('end', function(){
					assert.equal(body, '[]');
					done();
				});
				assert.equal(res.statusCode, 200);
				assert.equal(res.headers['content-type'], 'application/json');
			}).end();
		});
	});

	describe('when bundles exist', function(){
		before(function(done){
			var dir = '';
			path.dirname(config.dataDir+filename).split(path.sep).forEach(function(folder){
				dir += folder + path.sep;
				if(!fs.existsSync(dir)){
					fs.mkdirSync(dir);
				}
			})
			fs.writeFileSync(config.dataDir+filename, '[Mon Jan 28 22:16:56.421 2013] [478] INFO(bjnpluginAPI.cpp:756): JSLog: INFO  ♦ [Alphaomega.js:103][IDENTIFICATION] MeetingID: 11111\n[Mon Jan 28 22:16:56.421 2013] [478] INFO(bjnpluginAPI.cpp:756): JSLog: DEBUG  ♦ [analytics.js:60]Pushing GA event: [inmeeting-behaviors,elements,changedDefaultDevices,null]\n[Mon Jan 28 22:16:56.437 2013] [1790] INFO(skinnysipmanager.cpp:117): webrtc_voe_dev  ..Setting capture index 0\n[Mon Jan 28 22:16:57.530 2013] [478] INFO(bjnpluginAPI.cpp:756): JSLog: INFO  ♦ [Alphaomega.js:124][IDENTIFICATION] ParticipantGUID: 272ad0a7-e1a7-4973-ad56-a8a7a8281930\n[Mon Jan 28 22:16:57.530 2013] [478] INFO(bjnpluginAPI.cpp:756): JSLog: DEBUG  ♦ [Alphaomega.js:126]Pairing success with pairing code sip:TBUHEA@sj.a.bjn.vc;transport=TLS with new guid 272ad0a7-e1a7-4973-ad56-a8a7a8281930\n[Mon Jan 28 22:16:57.530 2013] [478] INFO(bjnpluginAPI.cpp:756): JSLog: DEBUG  ♦ [Alphaomega.js:130]11111,4481,a.bjn.vc\n[Mon Jan 28 22:16:57.530 2013] [478] INFO(bjnpluginAPI.cpp:756): JSLog: DEBUG  ♦ [mediaControllerImpl.js:577]making call from mediacontroller\n[Mon Jan 28 22:16:57.530 2013] [478] INFO(bjnpluginAPI.cpp:756): JSLog: DEBUG  ♦ [mediaControllerImpl.js:240]WEBAPP CALLING PEER CONNECTION FUNCTION makeCallWithURI2\n[Mon Jan 28 22:16:57.530 2013] [478] INFO(PeerConnectionAPI.cpp:396): PeerConnectionAPI::makeCallWithURI2\n[Mon Jan 28 22:16:57.530 2013] [478] INFO(PeerConnectionAPI.cpp:401): name: Ben uri: sip:TBUHEA@sj.a.bjn.vc;transport=TLS\n[Mon Jan 28 22:16:57.546 2013] [478] INFO(PeerConnectionAPI.cpp:416): TurnServerInfo: address: 199.48.152.52 protocol: udp port: 5000 username: go5iiGaiIeL0quah password: rei5Ahqucah3aW6u\n[Mon Jan 28 22:16:57.546 2013] [478] INFO(PeerConnectionAPI.cpp:416): TurnServerInfo: address: 199.48.152.52 protocol: tcp port: 5000 username: go5iiGaiIeL0quah password: rei5Ahqucah3aW6u\n[Mon Jan 28 22:16:57.546 2013] [478] INFO(PeerConnectionAPI.cpp:416): TurnServerInfo: address: 199.48.152.52 protocol: tcp port: 443 username: go5iiGaiIeL0quah password: rei5Ahqucah3aW6u\n[Mon Jan 28 22:16:57.546 2013] [478] INFO(PeerConnectionAPI.cpp:375): PeerConnectionAPI::makeCallWithURI_internal\n[Mon Jan 28 22:16:57.608 2013] [1790] INFO(skinnysipmanager.cpp:117): webrtc_voe_dev  ..Using audio layer 1\n[Mon Jan 28 22:16:57.608 2013] [1790] INFO(skinnysipmanager.cpp:117): webrtc_voe_dev  ..Vad is enabled with value 1\n[Mon Jan 28 22:16:57.608 2013] [1790] INFO(skinnysipmanager.cpp:117): webrtc_voe_dev  ..In function webrtc_voe_stream_start)\n', 'utf8');

			http.request({ port: config.apiPort, method: 'PUT', path: '/files/'+filename }, function(res){
				done();
			}).end();
		});
		
		it('lists the new bundle', function(done){
			http.request({ port: config.apiPort, method: 'GET', path: '/bundles' }, function(res){
				var body = '';
				res.on('data', function(chunk){ body += chunk; });
				res.on('end', function(){
					assert.deepEqual(JSON.parse(body), [{
						_id: catchmark,
						timestamp: 1360114618.123450,
						endpoint: 'acid',
						email: 'ben@aldaviva.com'
					}]);
					assert(res.headers['content-range'], 'Missing Content-Range header');
					assert.equal(res.headers['content-range'], '0-0/1');
					done();
				});
				assert.equal(res.statusCode, 200);
				assert.equal(res.headers['content-type'], 'application/json');
			}).end();
		});

		it('returns the new bundle', function(done){
			http.request({ port: config.apiPort, method: 'GET', path: '/bundles/'+catchmark }, function(res){
				var body = '';
				res.on('data', function(chunk){ body += chunk; });
				res.on('end', function(){
					var expected = {
						_id: catchmark,
						timestamp: 1360114618.123450,
						endpoint: 'acid',
						email: 'ben@aldaviva.com',
						meetings: [],
						legs: ["272ad0a7-e1a7-4973-ad56-a8a7a8281930"],
						comment: "",
						files: [{
							name    : 'rbjnplugin.1359440216_3864.log',
							local   : true,
							scanned : true
						}]
					};
					var actual = JSON.parse(body);
					assert.deepEqual(actual, expected, "ACTUAL: "+JSON.stringify(actual)+",\nEXPECTED "+JSON.stringify(expected));
					done();
				});
				assert.equal(res.statusCode, 200);
				assert.equal(res.headers['content-type'], 'application/json');
			}).end();
		});

		it('returns 404 for missing bundles', function(done){
			http.request({ port: config.apiPort, method: 'GET', path: '/bundles/00000000-0000-0000-0000-000000000000' }, function(res){
				assert.equal(res.statusCode, 404);
				done();
			}).end();
		});

	});

	describe('pagination', function(){
		before(function(done){
			bundles.insert([{
				_id: '88888888-8888-8888-8888-888888888888',
				timestamp: 1361175535,
				email: 'ben+test@bluejeans.com',
				comment: 'hello',
				endpoint: 'acid',
				legs: [],
				meetings: [],
				files: []
			},{
				_id: '99999999-9999-9999-9999-999999999999',
				timestamp: 1361830524,
				email: 'ben+test2@bluejeans.com',
				comment: 'world',
				endpoint: 'acid',
				legs: [],
				meetings: [],
				files: []
			}], function(err, insertedDocs){
				done();
			});
		});

		it('lists both bundles, sorted by date (newest first)', function(done){
			http.request({ port: config.apiPort, method: 'GET', path: '/bundles?sort=-timestamp' }, function(res){
				var body = '';
				res.on('data', function(chunk){ body += chunk; });
				res.on('end', function(){
					var bodyArray = JSON.parse(body);
					assert.equal(bodyArray[0]._id, '99999999-9999-9999-9999-999999999999', "Sort order is wrong");
					assert.equal(bodyArray[1]._id, '88888888-8888-8888-8888-888888888888', "Sort order is wrong");
					assert.equal(bodyArray[2]._id, catchmark, "Sort order is wrong");
					assert(res.headers['content-range'], 'Missing Content-Range header');
					assert.equal(res.headers['content-range'], '0-2/3');
					done();
				});
				assert.equal(res.statusCode, 200);
			}).end();
		});

		it('lists both bundles, sorted by date (oldest first)', function(done){
			http.request({ port: config.apiPort, method: 'GET', path: '/bundles?sort=+timestamp,-email' }, function(res){
				var body = '';
				res.on('data', function(chunk){ body += chunk; });
				res.on('end', function(){
					var bodyArray = JSON.parse(body);
					assert.equal(bodyArray[0]._id, catchmark, "Sort order is wrong");
					assert.equal(bodyArray[1]._id, '88888888-8888-8888-8888-888888888888', "Sort order is wrong");
					assert.equal(bodyArray[2]._id, '99999999-9999-9999-9999-999999999999', "Sort order is wrong");
					assert(res.headers['content-range'], 'Missing Content-Range header');
					assert.equal(res.headers['content-range'], '0-2/3');
					done();
				});
				assert.equal(res.statusCode, 200);
			}).end();
		});

		it('limits results', function(done){
			http.request({
				port: config.apiPort,
				method: 'GET',
				path: '/bundles?sort=-timestamp',
				headers: { 'Range': 'items=0-1' }
			}, function(res){
				var body = '';
				res.on('data', function(chunk){ body += chunk; });
				res.on('end', function(){
					var bodyArray = JSON.parse(body);
					assert.equal(bodyArray.length, 2, 'Expected 2 results, got '+bodyArray.length);
					assert.equal(bodyArray[0]._id, '99999999-9999-9999-9999-999999999999', "Sort order is wrong");
					assert.equal(bodyArray[1]._id, '88888888-8888-8888-8888-888888888888', "Sort order is wrong");
					assert(res.headers['content-range'], 'Missing Content-Range header');
					assert.equal(res.headers['content-range'], '0-1/3');
					done();
				});
				assert.equal(res.statusCode, 200);
			}).end();
		});
	});

	describe('filtering', function(){
		it('returns records with matching email address', function(done){
			http.request({ port: config.apiPort, method: 'GET', path: '/bundles?email=test2' }, function(res){
				var body = '';
				res.on('data', function(chunk){ body += chunk; });
				res.on('end', function(){
					var bodyArray = JSON.parse(body);
					assert.equal(bodyArray.length, 1, 'Expected 1 result, got '+bodyArray.length);
					assert.equal(bodyArray[0]._id, '99999999-9999-9999-9999-999999999999', "Filtered wrong records");
					assert(res.headers['content-range'], 'Missing Content-Range header');
					assert.equal(res.headers['content-range'], '0-0/1');
					done();
				});
				assert.equal(res.statusCode, 200);
			}).end();
		});

		it('returns records with matching timestamp', function(done){
			var timestamp = 0|new Date(2013, 1, 18).getTime()/1000;
			http.request({ port: config.apiPort, method: 'GET', path: '/bundles?timestamp='+timestamp }, function(res){
				var body = '';
				res.on('data', function(chunk){ body += chunk; });
				res.on('end', function(){
					var bodyArray = JSON.parse(body);
					assert.equal(bodyArray.length, 1, 'Expected 1 result, got '+bodyArray.length);
					assert.equal(bodyArray[0]._id, '88888888-8888-8888-8888-888888888888', "Filtered wrong records");
					assert(res.headers['content-range'], 'Missing Content-Range header');
					assert.equal(res.headers['content-range'], '0-0/1');
					done();
				});
				assert.equal(res.statusCode, 200);
			}).end();
		});
	});
});