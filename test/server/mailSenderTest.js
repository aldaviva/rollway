var testConfig         = require('../testConfig');
var config             = require(process.env.ROLLWAY_COVERAGE ? '../../lib-cov/config' : '../../lib/config').init(testConfig);
var assert             = require('assert');
var mailSender         = require(process.env.ROLLWAY_COVERAGE ? '../../lib-cov/mailSender' : '../../lib/mailSender');
var sinon              = require('sinon');
var smtpc              = require('smtpc');
var preferencesManager = require(process.env.ROLLWAY_COVERAGE ? '../../lib-cov/preferencesManager' : '../../lib/preferencesManager');

require('sinon-mocha').enhance(sinon);

describe('MailSender', function(){
	describe('.sendMail()', function(){

		var stubSmtpcSendmail;
		var stubPreferencesManagerGetPreferenceById;

		before(function(){
			stubPreferencesManagerGetPreferenceById = sinon.stub(preferencesManager, "getPreferenceById");
		});
 
		it("crafts messages correctly", function(done){
			stubSmtpcSendmail = sinon.stub(smtpc, "sendmail");
			stubSmtpcSendmail.yieldsTo("success");
			stubPreferencesManagerGetPreferenceById.withArgs("uploadNotifications").returns({
				_version: 0,
				recipients: ['ben@bluejeans.com', 'ben@bluejeansnet.com'],
				capsLock: false
			});

			mailSender.sendMail("acid/Foo Bar/1234567890.123456/call.log.1")
				.then(function(){
					sinon.assert.calledWith(stubSmtpcSendmail, sinon.match({
						host    : testConfig.smtpHost,
						port    : testConfig.smtpPort,
						from    : testConfig.smtpFrom,
						to      : ['ben@bluejeans.com', 'ben@bluejeansnet.com'],
						content : sinon.match.string
							.and(sinon.match("From: "+testConfig.smtpFrom))
							.and(sinon.match("To: ben@bluejeans.com, ben@bluejeansnet.com"))
							.and(sinon.match("Subject: Foo Bar uploaded an Acid log file"))
							.and(sinon.match("Foo Bar uploaded an Acid log file."))
							.and(sinon.match("http://logupload.bluejeans.com:8080/#bundle=3D6c8b6044-7e55-1adc-6d0d-b2cd3=\r\n03454cd"))
							.and(sinon.match('<div style=3D=22text-transform: none;=22><p><strong>Foo Bar</strong> =\r\nuploaded <a href=3D=22http://logupload.bluejeans.com:8080/#bundle=3D6c8b604=\r\n4-7e55-1adc-6d0d-b2cd303454cd=22>an Acid log file</a>.</p><p><a =\r\nhref=3D=22http://logupload.bluejeans.com:8080/#preferences=22 =\r\nstyle=3D=22font-style: italic; font-size: 0.81em;>Notification =\r\nsettings</a></p></div>'))
					}));
					done();
				}).done();
		});

		it("noop if no recipients are specified", function(done){
			stubSmtpcSendmail = sinon.stub(smtpc, "sendmail");
			stubSmtpcSendmail.yieldsTo("success");

			stubPreferencesManagerGetPreferenceById.withArgs("uploadNotifications").returns({
				_version: 0,
				recipients: [],
				capsLock: false
			});

			mailSender.sendMail("acid/Foo Bar/1234567890.123456/call.log.1")
				.then(function(){
					sinon.assert.notCalled(stubSmtpcSendmail);
					done();
				}).done();
		});

		after(function(){
			stubPreferencesManagerGetPreferenceById.restore();
		});
	});
});