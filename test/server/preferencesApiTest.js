var mongo        = require('mongodb');
var testConfig   = require('../testConfig');
var config       = require(process.env.ROLLWAY_COVERAGE ? '../../lib-cov/config' : '../../lib/config').init(testConfig);
var assert       = require('assert');
var rollway      = require('../..');
var http         = require('http');

var db;
var preferencesCollection;

// Keep-Alive socket limits in Node 0.10.0
// https://github.com/joyent/node/issues/4769
http.globalAgent.maxSockets = 65535;

describe('Preferences API', function(){

	before(function(done){
		rollway.startedPromise
			.then(function(){
				db = new mongo.Db(config.dbName, new mongo.Server(config.dbHost, config.dbPort), {
					journal: true
				});
				db.open(function(){
					db.collection('preferences', function(err, preferencesCollection_){
						preferencesCollection = preferencesCollection_;
						preferencesCollection.remove({}, function(err, recordsRemoved){
							done();
						});
					});
				});
			});
	});

	describe('returns defaults when none are set', function(){

		it('while listing all preferences', function(done){
			http.request({ method: 'GET', path: '/preferences', port: config.apiPort }, function(res){
				var body = '';
				res.on('data', function(chunk){
					body += chunk;
				});
				res.on('end', function(){
					assert.deepEqual(JSON.parse(body), {
						uploadNotifications: {
							_version: 0,
							recipients: [],
							capsLock: false
						}
					});
					done();
				});
				assert.equal(res.statusCode, 200);
				assert.equal(res.headers['content-type'], 'application/json');
			}).end();
		});

		it('while requesting specific preferences', function(done){
			http.request({ method: 'GET', path: '/preferences/uploadNotifications', port: config.apiPort }, function(res){
				var body = '';
				res.on('data', function(chunk){
					body += chunk;
				});
				res.on('end', function(){
					assert.deepEqual(JSON.parse(body), {
						_version: 0,
						recipients: [],
						capsLock: false
					});
					done();
				});
				assert.equal(res.statusCode, 200);
				assert.equal(res.headers['content-type'], 'application/json');
			}).end();
		});

	});

	describe('returns persisted preferences', function(){

		before(function(done){
			preferencesCollection.insert({
				_id: 'uploadNotifications',
				_version: 1,
				recipients: [ 'ben@bluejeans.com' ],
				capsLock: true
			}, function(err, result){
				if(err !== null) throw err;
				done();
			});
		});

		it('while listing all preferences', function(done){
			http.request({ method: 'GET', path: '/preferences', port: config.apiPort }, function(res){
				var body = '';
				res.on('data', function(chunk){
					body += chunk;
				});
				res.on('end', function(){
					assert.deepEqual(JSON.parse(body), {
						uploadNotifications: {
							_version: 1,
							recipients: [ 'ben@bluejeans.com' ],
							capsLock: true
						}
					});
					done();
				});
				assert.equal(res.statusCode, 200);
				assert.equal(res.headers['content-type'], 'application/json');
			}).end();
		});

		it('while requesting specific preferences', function(done){
			http.request({ method: 'GET', path: '/preferences/uploadNotifications', port: config.apiPort }, function(res){
				var body = '';
				res.on('data', function(chunk){
					body += chunk;
				});
				res.on('end', function(){
					assert.deepEqual(JSON.parse(body), {
						_version: 1,
						recipients: [ 'ben@bluejeans.com' ],
						capsLock: true 
					});
					done();
				});
				assert.equal(res.statusCode, 200);
				assert.equal(res.headers['content-type'], 'application/json');
			}).end();
		});
	});

	describe('setting preferences', function(){

		it('returns 204', function(done){
			var req = http.request({ method: 'PUT', path: '/preferences/uploadNotifications', port: config.apiPort, headers: { 'content-type': 'application/json' }}, function(res){
				assert.equal(res.statusCode, 204);
				done();
			});
			req.write(JSON.stringify({
				_version: 1,
				recipients: ['ben@bluejeans.com'],
				capsLock: false
			}));
			req.end();
		});

		it('persists the requested values', function(done){
			preferencesCollection.findOne({_id: 'uploadNotifications'}, function(err, doc){
				assert.equal(err, null);
				assert.deepEqual(doc, {
					_id: 'uploadNotifications',
					_version: 2,
					recipients: ['ben@bluejeans.com'],
					capsLock: false
				});
				done();
			});
		});

		describe('based on an out-of-date version', function(){

			it('returns 409', function(done){
				var req = http.request({ method: 'PUT', path: '/preferences/uploadNotifications', port: config.apiPort, headers: { 'content-type': 'application/json' }}, function(res){
					assert.equal(res.statusCode, 409);
					done();
				});
				req.write(JSON.stringify({
					_version: 0,
					recipients: ['fail@bluejeans.com'],
					capsLock: true
				}));
				req.end();
			});

			it('leaves the database the hell alone', function(done){
				preferencesCollection.findOne({_id: 'uploadNotifications'}, function(err, doc){
					assert.equal(err, null);
					assert.deepEqual(doc, {
						_id: 'uploadNotifications',
						_version: 2,
						recipients: ['ben@bluejeans.com'],
						capsLock: false
					});
					done();
				});
			});
			
		});

	});

	after(function(done){
		preferencesCollection.remove({}, function(err, recordsRemoved){
			done();
		});
	});

});