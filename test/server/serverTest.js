var testConfig   = require('../testConfig');
var config       = require(process.env.ROLLWAY_COVERAGE ? '../../lib-cov/config' : '../../lib/config').init(testConfig);
var assert       = require('assert');
var rollway      = require('../..');
var http         = require('http');

// Keep-Alive socket limits in Node 0.10.0
// https://github.com/joyent/node/issues/4769
http.globalAgent.maxSockets = 65535;

describe('HTTP server', function(){
	before(function(done){
		rollway.startedPromise
			.then(function(){
				done();
			});
	});

	describe('heartbeat', function(){
		it('responds with 204', function(done){
			http.request({ method: 'GET', path: '/ruok', port: config.apiPort }, function(res){
				assert.equal(res.statusCode, 204);
				done();
			}).end();
		});
	});
});