var fs           = require('fs');
var path         = require('path');
var mongo        = require('mongodb');
var testConfig   = require('../testConfig');
var config       = require(process.env.ROLLWAY_COVERAGE ? '../../lib-cov/config' : '../../lib/config').init(testConfig);
var assert       = require('assert');
var rollway      = require('../..');
var http         = require('http');

var db;
var bundles;

// Keep-Alive socket limits in Node 0.10.0
// https://github.com/joyent/node/issues/4769
http.globalAgent.maxSockets = 65535;

describe('File API', function(){
	var filename = 'acid/Ben Hutchison/1360114618.123450/rbjnplugin.1359440216_3864.log';
	var catchmark = '4793afc5-b6df-d385-7ecc-d8c9c89443f2';
	// var catchmark = '107af660-231e-828b-edb7-8d9e4dee5346'; //integer timestamp
	// var filename = 'Browser/ben@aldaviva.com/1360114618.123450/rbjnplugin.1359440216_3864.log';
	// var catchmark = '836b2316-390f-e55f-1405-0748c95fa645';

	before(function(done){
		rollway.startedPromise
			.then(function(){
				db = new mongo.Db(config.dbName, new mongo.Server(config.dbHost, config.dbPort), {
					journal: true
				});
				db.open(function(){
					db.collection('bundles', function(err, bundles_){
						bundles = bundles_;
						bundles.remove({}, function(err, recordsRemoved){
							done();
						});
					});
				});
			});
	});

	describe('before any files are added', function(){
		it('returns an empty array', function(done){
			http.request({ method: 'GET', path: '/files', port: config.apiPort }, function(res){
				var body = '';
				res.on('data', function(chunk){
					body += chunk;
				});
				res.on('end', function(){
					assert.equal(body, '[]');
					done();
				});
				assert.equal(res.statusCode, 200);
				assert.equal(res.headers['content-type'], 'application/json');
			}).end();
		});
	});

	describe('adding a file', function(){
		before(function(){
			var dir = '';
			path.dirname(config.dataDir+filename).split(path.sep).forEach(function(folder){
				dir += folder + path.sep;
				if(!fs.existsSync(dir)){
					fs.mkdirSync(dir);
				}
			});
			fs.writeFileSync(config.dataDir+filename, '[Mon Jan 28 22:16:56.421 2013] [478] INFO(bjnpluginAPI.cpp:756): JSLog: INFO  ♦ [Alphaomega.js:103][IDENTIFICATION] MeetingID: 11111[Mon Jan 28 22:16:56.421 2013] [478] INFO(bjnpluginAPI.cpp:756): JSLog: DEBUG  ♦ [analytics.js:60]Pushing GA event: [inmeeting-behaviors,elements,changedDefaultDevices,null][Mon Jan 28 22:16:56.437 2013] [1790] INFO(skinnysipmanager.cpp:117): webrtc_voe_dev  ..Setting capture index 0[Mon Jan 28 22:16:57.530 2013] [478] INFO(bjnpluginAPI.cpp:756): JSLog: INFO  ♦ [Alphaomega.js:124][IDENTIFICATION] ParticipantGUID: 272ad0a7-e1a7-4973-ad56-a8a7a8281930[Mon Jan 28 22:16:57.530 2013] [478] INFO(bjnpluginAPI.cpp:756): JSLog: INFO  ♦ [Alphaomega.js:124][IDENTIFICATION] MeetingGUID: 123456:272407-08192317-2d56-49a8-a404-da6c106f46b5\n[Mon Jan 28 22:16:57.530 2013] [478] INFO(bjnpluginAPI.cpp:756): JSLog: DEBUG  ♦ [Alphaomega.js:126]Pairing success with pairing code sip:TBUHEA@sj.a.bjn.vc;transport=TLS with new guid 272ad0a7-e1a7-4973-ad56-a8a7a8281930[Mon Jan 28 22:16:57.530 2013] [478] INFO(bjnpluginAPI.cpp:756): JSLog: DEBUG  ♦ [Alphaomega.js:130]11111,4481,a.bjn.vc[Mon Jan 28 22:16:57.530 2013] [478] INFO(bjnpluginAPI.cpp:756): JSLog: DEBUG  ♦ [mediaControllerImpl.js:577]making call from mediacontroller[Mon Jan 28 22:16:57.530 2013] [478] INFO(bjnpluginAPI.cpp:756): JSLog: DEBUG  ♦ [mediaControllerImpl.js:240]WEBAPP CALLING PEER CONNECTION FUNCTION makeCallWithURI2[Mon Jan 28 22:16:57.530 2013] [478] INFO(PeerConnectionAPI.cpp:396): PeerConnectionAPI::makeCallWithURI2[Mon Jan 28 22:16:57.530 2013] [478] INFO(PeerConnectionAPI.cpp:401): name: Ben uri: sip:TBUHEA@sj.a.bjn.vc;transport=TLS[Mon Jan 28 22:16:57.546 2013] [478] INFO(PeerConnectionAPI.cpp:416): TurnServerInfo: address: 199.48.152.52 protocol: udp port: 5000 username: go5iiGaiIeL0quah password: rei5Ahqucah3aW6u[Mon Jan 28 22:16:57.546 2013] [478] INFO(PeerConnectionAPI.cpp:416): TurnServerInfo: address: 199.48.152.52 protocol: tcp port: 5000 username: go5iiGaiIeL0quah password: rei5Ahqucah3aW6u[Mon Jan 28 22:16:57.546 2013] [478] INFO(PeerConnectionAPI.cpp:416): TurnServerInfo: address: 199.48.152.52 protocol: tcp port: 443 username: go5iiGaiIeL0quah password: rei5Ahqucah3aW6u[Mon Jan 28 22:16:57.546 2013] [478] INFO(PeerConnectionAPI.cpp:375): PeerConnectionAPI::makeCallWithURI_internal[Mon Jan 28 22:16:57.608 2013] [1790] INFO(skinnysipmanager.cpp:117): webrtc_voe_dev  ..Using audio layer 1[Mon Jan 28 22:16:57.608 2013] [1790] INFO(skinnysipmanager.cpp:117): webrtc_voe_dev  ..Vad is enabled with value 1[Mon Jan 28 22:16:57.608 2013] [1790] INFO(skinnysipmanager.cpp:117): webrtc_voe_dev  ..In function webrtc_voe_stream_start)', 'utf8');
		});

		it('accepts PUT request with the new filename', function(done){
			var path = encodeURI('/files/'+filename);
			http.request({ port: config.apiPort, method: 'PUT', path: path }, function(res){
				var body = '';
				res.on('data', function(chunk){ body += chunk; });
				res.on('end', function(){
					assert.equal(res.statusCode, 204, body);
					done();
				});
			}).end();
		});

		it('adds the correct document to the database', function(done){
			bundles.find({}, function(err, cursor){
				cursor.toArray(function(err, documents){
					assert.equal(err, null);
					assert.equal(documents.length, 1, "db should have 1 document, has "+documents.length);
					var expected = {
						_id       : catchmark,
						comment   : '',
						email     : 'Ben Hutchison',
						endpoint  : 'acid',
						timestamp : 1360114618.123450,
						legs      : ["272ad0a7-e1a7-4973-ad56-a8a7a8281930"],
						meetings  : ["08192317-2d56-49a8-a404-da6c106f46b5"],
						files     : [{
							name    : 'rbjnplugin.1359440216_3864.log',
							local   : true,
							scanned : true
						}]
					};
					assert.deepEqual(documents[0], expected, "expected "+JSON.stringify(expected)+", got "+JSON.stringify(documents[0]));
					cursor.close();
					done();
				});
			});
		});

		it('adds the catchmark', function(){
			var fileContents = fs.readFileSync(config.dataDir+filename, { encoding: 'utf8' });
			var expectedCatchmark = 'Rollway catchmark: '+catchmark;
			assert.equal(fileContents.indexOf(expectedCatchmark), fileContents.length - expectedCatchmark.length, "file does not end with catchmark");

			var catchmarkOccurrences = fileContents.match(/Rollway catchmark: [0-9a-f\-]{36}/g);
			assert.equal(catchmarkOccurrences.length, 1, "Expected one catchmark, found "+catchmarkOccurrences.length);
		});

		it('returns 304 for files that have already been added', function(done){
			http.request({ port: config.apiPort, method: 'PUT', path: '/files/'+encodeURI(filename) }, function(res){
				assert.equal(res.statusCode, 304);
				done();
			}).end();
		});

		it('only adds at most one catchmark to a file', function(done){
			var filename2 = filename+'2';
			fs.renameSync(config.dataDir+filename, config.dataDir+filename2);

			//move file on disk to a different filename, as if we cleared the database and reindexed
			//issue PUT request for new filename to try to get rollway to add a second catchmark

			http.request({ port: config.apiPort, method: 'PUT', path: '/files/'+encodeURI(filename2) }, function(res){
				assert.equal(res.statusCode, 204);

				var fileContents = fs.readFileSync(config.dataDir+filename2, { encoding: 'utf8' });
				var catchmarkOccurrences = fileContents.match(/Rollway catchmark: [0-9a-f\-]{36}/g);
				assert.equal(catchmarkOccurrences.length, 1, "Expected one catchmark, found "+catchmarkOccurrences.length);
				fs.renameSync(config.dataDir+filename2, config.dataDir+filename);

				bundles.update({ "_id": catchmark }, { $pop: { files: 1 }}, function(err){
					done();
				});

			}).end();
		});
	});

	describe('listing files', function(){
		it('lists the new filename', function(done){
			http.request({ port: config.apiPort, method: 'GET', path: '/files' }, function(res){
				var body = '';
				res.on('data', function(chunk){ body += chunk; });
				res.on('end', function(){
					assert.equal(body, '["'+encodeURI(filename)+'"]');
					done();
				});
				assert.equal(res.statusCode, 200);
				assert.equal(res.headers['content-type'], 'application/json');
			}).end();
		});

		describe('with limits', function(){
			var hugeBundleId = 'acbd18db-4cc2-f85c-edef-654fccc4a4d8';

			before(function(done){
				var hugeBundle = {
					_id: hugeBundleId,
					files: [],
					comment: '',
					endpoint: 'acid',
					email: 'ben@aldaviva.com',
					timestamp: 1363728963.177517,
					legs: [],
					meetings: []
				};
				for(var fileIdx=0; fileIdx < 100; ++fileIdx){
					hugeBundle.files.push({ name: 'file'+fileIdx+'.log', scanned: true, local: true });
				}
				bundles.insert(hugeBundle, {}, done);
			});

			it('returns at most 32 files by default', function(done){
				http.request({ port: config.apiPort, method: 'GET', path: '/files' }, function(res){
					var body = '';
					res.on('data', function(chunk){ body += chunk; });
					res.on('end', function(){
						var bodyObj = JSON.parse(body);
						assert(bodyObj instanceof Array);
						assert.equal(bodyObj.length, 32, "expected an array with 32 items, but got "+bodyObj.length+" items");
						done();
					});
					assert.equal(res.statusCode, 200);
					assert.equal(res.headers['content-type'], 'application/json');
				}).end();
			});

			after(function(done){
				bundles.remove({ _id: hugeBundleId }, {}, done);
			});
		});
	});

	describe('adding a comment', function(){
		var comment = "Hello world!";
		var commentDir = path.dirname(filename);

		before(function(){
			fs.writeFileSync(config.dataDir+commentDir+'/comment.txt', comment, 'utf8');
		});

		it('accepts PUT request with the comment file', function(done){
			http.request({ port: config.apiPort, path: encodeURI('/files/'+commentDir+'/comment.txt'), method: 'PUT' }, function(res){
				assert.equal(res.statusCode, 204);
				done();
			}).end();
		});

		it('adds the comment to the database', function(done){
			bundles.findOne({ _id: catchmark }, function(err, doc){
				assert.equal(doc.comment, comment);
				done();
			});
		});

		it('removes the comment file from filesystem', function(){
			var path = config.dataDir+commentDir+'/comment.txt';
			assert(!fs.existsSync(path), "should not exist: "+path);
		});
	});

	describe('adding a binary file', function(){
		var binaryFilename = 'acid/ben@aldaviva.com/1360114618.123450/empty.gif';
		var binaryDataHex = '4749463839610100010080010000000000000021f90401000001002c00000000010001000002024c01003b';

		before(function(){
			fs.writeFileSync(config.dataDir+binaryFilename, binaryDataHex, { encoding: 'hex', mode: 'w' });
		});

		it('refuses PUT request', function(done){
			http.request({ port: config.apiPort, method: 'PUT', path: '/files/'+binaryFilename }, function(res){
				assert.equal(res.statusCode, 403, "expected 403 for skipping binary file, but got "+res.statusCode);
				done();
			}).end();
		});

		it('does not list the new filename', function(done){
			http.request({ port: config.apiPort, method: 'GET', path: '/files' }, function(res){
				var body = '';
				res.on('data', function(chunk){ body += chunk; });
				res.on('end', function(){
					assert.strictEqual(body.indexOf(binaryFilename), -1, "file listing should not return binary files");
					done();
				});
				assert.equal(res.statusCode, 200);
				assert.equal(res.headers['content-type'], 'application/json');
			}).end();
		});

		it('does not add a document to the database', function(done){
			bundles.find({}, function(err, cursor){
				cursor.toArray(function(err, documents){
					assert.equal(err, null);
					assert.equal(documents.length, 1, "db should have 1 document, has "+documents.length);
					documents[0].files.forEach(function(f){
						assert.notEqual(f.name, path.basename(binaryFilename), "binary file should not have been added to database");
					});
					cursor.close();
					done();
				});
			});
		});

		it('does not add the catchmark', function(){
			var fileContents = fs.readFileSync(config.dataDir+binaryFilename, { encoding: 'utf8' });

			var catchmarkOccurrences = fileContents.match(/Rollway catchmark: [0-9a-f\-]{36}/g);
			assert.strictEqual(catchmarkOccurrences, null, "Binary file should not have a catchmark added.");
		});

		after(function(){
			// fs.unlinkSync(config.dataDir+binaryFilename); //TODO
		});

		describe("that is missing from disk", function(){
			it("returns 404", function(done){
				http.request({ port: config.apiPort, method: 'PUT', path: '/files/'+binaryFilename+"FAKE" }, function(res){
					assert.equal(res.statusCode, 404, "expected 404 for skipping binary file, but got "+res.statusCode);
					done();
				}).end();
			});
		});
	});

	describe('downloading a file', function(){
		it('serves existing files', function(done){
			http.request({ port: config.apiPort, method: 'GET', path: encodeURI('/files/'+filename) }, function(res){
				var body = '';
				res.on('data', function(chunk){ body += chunk; });
				res.on('end', function(){
					var expectedCatchmark = 'Rollway catchmark: '+catchmark;
					assert.equal(body.indexOf(expectedCatchmark), body.length - expectedCatchmark.length);
					done();
				});
				assert.equal(res.statusCode, 200);
				assert.equal(res.headers['content-type'], 'text/plain');
			}).end();
		});

		it('returns 404 for missing files', function(done){
			http.request({ port: config.apiPort, method: 'GET', path: '/files/Browser/ben@aldaviva.com/1360114618/rbjnplugin.1359440216_3864_FAKE.log' }, function(res){
				assert.equal(res.statusCode, 404);
				done();
			}).end();
		});
	});

	describe('deleting a file', function(){
		it('deletes existing files', function(done){
			var req = http.request({ port: config.apiPort, path: '/files/batchDelete', method: 'POST', headers: { 'content-type': 'application/json' }}, function(res){
				assert.equal(res.statusCode, 204);
				assert.equal(false, fs.existsSync(config.dataDir+filename), 'file was not deleted from filesystem');
				done();
			});
			req.write('["'+encodeURI(filename)+'"]');
			req.end();
		});

		it('marks the file as non-local', function(done){
			bundles.findOne({ _id: catchmark }, function(err, doc){
				assert.equal(doc.files.length, 1, 'files array should have 1 file, instead found '+doc.files.length);
				var file = doc.files[0];
				assert.equal(file.local, false, 'expected file.local=false');
				done();
			});
		});

		it('handles missing files gracefully', function(done){
			var req = http.request({ port: config.apiPort, path: '/files/batchDelete', method: 'POST', headers: { 'content-type': 'application/json' }}, function(res){
				assert.equal(res.statusCode, 204);
				done();
			});
			req.write('["Browser/ben@aldaviva.com/1360114618/rbjnplugin.1359440216_3864_FAKE.log"]');
			req.end();
		});
	});
});