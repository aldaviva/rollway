var _                  = require('lodash');
var analyticsDb        = require('./database').analytics;
var analyticsMapReduce = require('./analyticsMapReduce');
var config             = require('./config');
var logger             = require('./logger')(__filename);
var Mediator           = require('mediator-js').Mediator;
var Q                  = require('q');

var exchange          = new Mediator();
var eventsCollection  = analyticsDb.collection('events');
var reportsCollection = analyticsDb.collection('reports');

var topics = {
	analytics: {
		reportsGenerated: true
	},
	event: {
		server: {
			scanVelocity: true
		},
		client: {
			fileViewed: true,
			indigoLinked: true,
			pageLoaded: true
		}
	}
};
module.exports.event = topics.event;
visitTopicNode(null, null, null, topics);

module.exports.publish = function(topic, data){
	topic = getTopic(topic);
	
	if(topic){
		var timestamp = new Date().getTime();
		var doc = _.extend({
			timestamp: timestamp,
			topic: topic //maybe superfluous
		}, data);
		
		if(topic != 'analytics:reportsGenerated'){
			insertEventIntoDatabase(doc);
		}

		exchange.publish(topic, doc);
	}
};

module.exports.subscribe = function(topic, callback){
	topic = getTopic(topic);
	return exchange.subscribe(topic, callback);
};

module.exports.generateReports = function(){
	var map        = analyticsMapReduce.map;
	var reduce     = analyticsMapReduce.reduce;
	var finalize   = analyticsMapReduce.finalize;
	var increments = analyticsMapReduce.increments;

	var minTimestamp = new Date().getTime() - (increments.month.incrSize * increments.month.incrCount);

	eventsCollection.mapReduce(map, reduce, {
		// scope: {
		// 	global: { //to make jsl happy
		// 		increments: analyticsMapReduce.increments
		// 	}
		// },
		scope: analyticsMapReduce.mongoScope,
		query: {
			timestamp: { $gte: minTimestamp } //TODO for incremental map reduce, we should filter by timestamp > generatedOn
		},
		sort: { timestamp: 1 },
		finalize: finalize,
		out: { replace: 'reports' }
	}, function(err, collection){
		if(err){
			logger.error("Could not mapReduce", err);
		} else {
			module.exports.publish(topics.analytics.reportsGenerated);
			logger.log("Reports generated.");
		}
	});
};

module.exports.getReportsByPeriod = function(periodName){
	if(!analyticsMapReduce.increments[periodName]){
		return Q.reject("Invalid report period. Try one of "+Object.keys(analyticsMapReduce.increments).join(', '));
	} else {
		return Q.ninvoke(reportsCollection, 'find', { _id: { $regex: '_'+periodName+'$' }})
			.then(function(cursor){
				return Q.ninvoke(cursor, 'toArray');
			});
	}
};

module.exports.getEmptyReport = function(reportId, periodName){
	var isScanVelocity = (reportId == 'event:server:scanVelocity');
	var now = new Date().getTime();
	var increment = analyticsMapReduce.increments[periodName];

	var report = {
		_id: reportId + '_' + periodName,
		value: {
			generatedOn: now,
			incrementCount: increment.incrCount,
			incrementSize: increment.incrSize,
			series: {}
		}
	};

	var mostRecentQuantizedTimestamp = analyticsMapReduce.quantizeTimestamp(now, increment.incrSize);

	for(var buckets = 0, timestamp = mostRecentQuantizedTimestamp; buckets < increment.incrCount; buckets++, timestamp -= increment.incrSize){
		if(isScanVelocity){
			report.value.series[timestamp] = {
				fileSizeBytes: 0,
				durationMillis: 0
			};
		} else {
			report.value.series[timestamp] = 0;
		}
	}

	if(isScanVelocity){
		report.value.totalDurationMillis = 0;
		report.value.totalFileSizeBytes = 0;
	} else {
		report.value.count = 0;
	}

	return report;
};

function insertEventIntoDatabase(doc){
	logger.log("inserting event ", doc);
	eventsCollection.insert(doc, { w: 0 }); //don't care
}

function getTopic(topic){
	/*if(typeof topic == 'function'){
		return topic();*/
	if(typeof topic == 'string'){
		return topic;
	} else if(typeof topic == 'undefined'){
		logger.warn("Invalid topic");
		return null;
	} else {
		return topic;
	}
}

//test at http://jsfiddle.net/J2NGN/
function visitTopicNode(parentNames, parentNode, name, node){
	var fullNodeName = parentNames + ':' + name;

	/*if(typeof node == 'function'){
		parentNode[name] = _.partial(node, fullNodeName);
	} else*/
	if(typeof node == 'boolean') {
		parentNode[name] = fullNodeName;
		/*function(){
			return fullNodeName;
		};*/
	} else {
		var recursiveParentName = (parentNames) ? fullNodeName : name;
		_.forOwn(node, function(childNode, childName){
			visitTopicNode(recursiveParentName, node, childName, childNode);
		});
	}
}

setInterval(module.exports.generateReports, 5*60*1000);
setTimeout(module.exports.generateReports, 3*1000);