var _                  = require('lodash');
var bundleManager      = require('./bundleManager');
var config             = require('./config');
var logger             = require('./logger')(__filename);
var MailComposer       = require("mailcomposer").MailComposer;
var preferencesManager = require('./preferencesManager');
var Q                  = require('q');
var smtpc              = require('smtpc');

require('d8');
require('d8/locale/en-US');

var EMAIL_PATTERN = /^[A-Za-z0-9._%+-]+@(?:[A-Za-z0-9-]+\.)+(?:[A-Za-z]{2}|aero|asia|biz|com|coop|edu|gov|info|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|xxx)$/;

//I hope this hardcoding doesn't make anyone's life harder in the future
var BASE_URL = 'http://logupload.bluejeans.com:8080/';

module.exports.sendMail = function(filePath){
	var pathInfo      = bundleManager.getPathInfo(filePath);
	var uploaderEmail = pathInfo.email;
	var endpointType  = pathInfo.endpoint;
	var catchmark     = pathInfo.catchmark;

	var recipients;
	var isCapsLock;

	return Q.try(function(){
			return preferencesManager.getPreferenceById('uploadNotifications');
		})
		.then(function(preferences){
			recipients = _.filter(preferences.recipients || [], function(email){
				return EMAIL_PATTERN.test(email);
			});
			isCapsLock = preferences.capsLock;

			if(!recipients.length){ throw 'no_recipients'; }
		})
		.then(function(){
			return buildMessageSource(uploaderEmail, endpointType, catchmark, recipients, isCapsLock);
		})
		.then(function(messageSource){
			return sendMessageToServer(messageSource, recipients);
		})
		.fail(function(err){
			if(err == 'no_recipients'){
				return;
			} else {
				logger.error("Could not send mail:", err);
			}
		});
};

function buildMessageSource(uploaderEmail, endpointType, catchmark, recipients, isCapsLock){

	var ensureCase = isCapsLock
		? function(str){ return str.toUpperCase(); }
		: _.identity;

	var mailComposer = new MailComposer();

	uploaderEmail = uploaderEmail || 'Somebody';

	var rollwayBundleLink = BASE_URL + '#bundle='+catchmark;
	var rollwayPreferencesLink = BASE_URL + '#preferences';

	var endpointPhrase = (endpointType == 'Browser' ? 'a Skinny log file' : (endpointType == 'acid' ? 'an Acid log file' : 'a ' + endpointType + ' log file'));

	var subject = ensureCase(uploaderEmail + ' uploaded ' + endpointPhrase);

	var textBody = ensureCase(uploaderEmail + ' uploaded ' + endpointPhrase) + '.\n' + rollwayBundleLink + ensureCase('\n\nNotification settings: ') + rollwayPreferencesLink;

	var htmlBody = '<div style="text-transform: ' + (isCapsLock ? 'uppercase' : 'none') + ';"><p><strong>' + uploaderEmail + '</strong> uploaded <a href="' + rollwayBundleLink + '">' + endpointPhrase + '</a>.</p><p><a href="' + rollwayPreferencesLink + '" style="font-style: italic; font-size: 0.81em;>Notification settings</a></p></div>';

	mailComposer.setMessageOption({
		from    : config.smtpFrom,
		to      : recipients,
		subject : subject,
		body    : textBody,
		html    : htmlBody
	});

	mailComposer.addHeader('date', (new Date()).format(Date.formats.RFC_2822));

	return Q.ninvoke(mailComposer, "buildMessage");
}

function sendMessageToServer(messageSource, recipients){
	var deferred = Q.defer();

	smtpc.sendmail({
		host    : config.smtpHost,
		port    : config.smtpPort,
		from    : config.smtpFrom,
		to      : recipients,
		content : messageSource,
		success : deferred.resolve,
		failure : deferred.reject
	});

	return deferred.promise;
}