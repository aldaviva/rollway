(function(){
	
	var analyticsMapReduce = {};

	analyticsMapReduce.increments = {
		year  : { incrSize: 1000*60*60*24*7, incrCount: 52  }, //52 weeks in a year
		month : { incrSize: 1000*60*60*24,   incrCount: 28  }, //28 days in a month
		week  : { incrSize: 1000*60*60*6,    incrCount: 7*4 }, //28 6-hour chunks in a week
		day   : { incrSize: 1000*60*60,      incrCount: 24  }, //24 hours in a day
		hour  : { incrSize: 1000*60,         incrCount: 60  }  //60 minutes in an hour
	};

	/**
	 * Map expects these to be in scope:
	 *  - global.increments: the increments dictionary above
	 *  - global.toMidnight: the toMidnight function in this module
	 *
	 * Mongo can't handle bound functions, so remember to include these in the
	 * opts.scope object in the call to collection.mapReduce(), or in the 
	 * closure of whereever you are manually map-reducing events.
	 */

	analyticsMapReduce.map = function(){
		function toMidnight(ts){
			var result = new Date(ts);
			result.setHours(0);
			result.setMinutes(0);
			result.setSeconds(0);
			result.setMilliseconds(0);
			return result.getTime();
		}
		function quantizeTimestamp(timestamp, incrSize){
			var midnight = toMidnight(timestamp);
			return Math.floor((timestamp - midnight) / incrSize) * incrSize + midnight;
		}

		function doEmit(topic, timestamp, periodName_, value){
			var series = {};
			series[timestamp] = value;
			emit(topic + '_' + periodName_, {
				series: series
			});
		}

		for(var periodName in global.increments){
			var incrementSize = global.increments[periodName].incrSize;

			var quantizedTimestamp = quantizeTimestamp(this.timestamp, incrementSize);

			if(this.topic == 'event:server:scanVelocity'){
				doEmit('event:server:bytesProcessed', quantizedTimestamp, periodName, this.fileSizeBytes);
				doEmit('event:server:fileProcessed', quantizedTimestamp, periodName, 1);
				doEmit('event:server:scanTime', quantizedTimestamp, periodName, this.durationMillis);
				doEmit(this.topic, quantizedTimestamp, periodName, {
					fileSizeBytes: this.fileSizeBytes,
					durationMillis: this.durationMillis
				});
			} else {
				doEmit(this.topic, quantizedTimestamp, periodName, 1);
			}
		}
	};

	analyticsMapReduce.reduce = function(key, values){
		var reduced = {
			series: {}
		};

		var isScanVelocity = (key.split('_')[0] == 'event:server:scanVelocity');

		values.forEach(function(value){
			for(var timestamp in value.series){
				if(isScanVelocity){
					var existingDatum;
					if(typeof reduced.series[timestamp] == 'undefined'){
						existingDatum = { fileSizeBytes: 0, durationMillis: 0 };
					} else {
						existingDatum = reduced.series[timestamp];
					}

					reduced.series[timestamp] = {
						fileSizeBytes: value.series[timestamp].fileSizeBytes + existingDatum.fileSizeBytes,
						durationMillis: value.series[timestamp].durationMillis + existingDatum.durationMillis
					};
				} else {
					reduced.series[timestamp] = value.series[timestamp] + (reduced.series[timestamp] || 0);
				}
			}
		});

		return reduced;
	};

	analyticsMapReduce.finalize = function(key, reducedValue){
		function toMidnight(ts){
			var result = new Date(ts);
			result.setHours(0);
			result.setMinutes(0);
			result.setSeconds(0);
			result.setMilliseconds(0);
			return result.getTime();
		}

		function quantizeTimestamp(ts, incrSize){
			var midnight = toMidnight(ts);
			return Math.floor((ts - midnight) / incrSize) * incrSize + midnight;
		}

		var now = new Date().getTime();
		var periodName = key.match(/_([^_]+?)$/)[1];
		var isScanVelocity = (key.split('_')[0] == 'event:server:scanVelocity');

		if(isScanVelocity){
			reducedValue.totalDurationMillis = 0;
			reducedValue.totalFileSizeBytes = 0;
		} else {
			reducedValue.count = 0;
		}

		var increment = global.increments[periodName];
		for(var quantizedTimestamp in reducedValue.series){
			if(quantizedTimestamp < (now - (increment.incrSize * increment.incrCount))){
				delete reducedValue.series[quantizedTimestamp];
			} else if(isScanVelocity){
				reducedValue.totalFileSizeBytes += reducedValue.series[quantizedTimestamp].fileSizeBytes;
				reducedValue.totalDurationMillis += reducedValue.series[quantizedTimestamp].durationMillis;
			} else {
				reducedValue.count += reducedValue.series[quantizedTimestamp];
			}
		}

		reducedValue.generatedOn = now;
		reducedValue.incrementSize = increment.incrSize;
		reducedValue.incrementCount = increment.incrCount;

		// var mostRecentMidnight = toMidnight(now);
		var mostRecentQuantizedTimestamp = quantizeTimestamp(now, increment.incrSize);

		for(var buckets = 0, timestamp = mostRecentQuantizedTimestamp; buckets < increment.incrCount; buckets++, timestamp -= increment.incrSize){
			if(typeof reducedValue.series[timestamp] == 'undefined'){
				if(isScanVelocity){
					reducedValue.series[timestamp] = {
						fileSizeBytes: 0,
						durationMillis: 0
					};
				} else {
					reducedValue.series[timestamp] = 0;
				}
			}
		}

		return reducedValue;
	};

	analyticsMapReduce.toMidnight = function(ts){
		var result = new Date(ts);
		result.setHours(0);
		result.setMinutes(0);
		result.setSeconds(0);
		result.setMilliseconds(0);
		return result.getTime();
	};

	analyticsMapReduce.quantizeTimestamp = function(timestamp, incrementSize){
		var midnight = analyticsMapReduce.toMidnight(timestamp);
		return Math.floor((timestamp - midnight) / incrementSize) * incrementSize + midnight;
	};

	analyticsMapReduce.mongoScope = {
		global: {
			increments: analyticsMapReduce.increments
		}
	};

	if(typeof module != 'undefined' && typeof module.exports != 'undefined'){
		module.exports = analyticsMapReduce;
	} else if(typeof define == 'function' && define.amd){
		define('analyticsMapReduce', [], analyticsMapReduce);
	} else {
		this.analyticsMapReduce = analyticsMapReduce;
	}

})();