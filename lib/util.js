/**
 * @param s "true", "false", "0", "1", "yes", "no"
 * @returns true, false
 */
Boolean.parseBoolean = function(s){
	switch((s||"0").toLowerCase()){
		case "true" : case "yes": case "1": return true;
		case "false": case "no" : case "0": return false;
		default: return Boolean(s);
	}
};