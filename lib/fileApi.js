var _             = require('lodash');
var apiServer     = require('./apiServer');
var bundleManager = require('./bundleManager');
var fileManager   = require('./fileManager');
var logger        = require('./logger')(__filename);
var mailSender    = require('./mailSender');
var path          = require('path');
var Q             = require('q');

apiServer.put({ path: /\/files\/(.+)/, name: "notifyNewFile" }, function(req, res){
	var filePath = decodeURI(req.params[0]);

	var promise;

	if(path.basename(filePath) == 'comment.txt'){
		promise = fileManager.getComments(filePath)
			.then(function(comment){
				return bundleManager.setComment(filePath, comment);
			})
			.then(function(){
				fileManager.delete(filePath);
			});

	} else {
		promise = Q.resolve().then(function(){
				return fileManager.ensureTextFile(filePath);
			})
			.then(function(){
				return bundleManager.addFile(filePath);
			})
			.then(function(){
				return fileManager.scanFile(filePath);
			})
			.then(function(){
				mailSender.sendMail(filePath);
				//don't care about whether email sending succeeded
			})
			.fail(function(err){
				if(typeof err == 'string'){
					if(err == 'exists'){
						res.send(304);
					} else if(err.indexOf('Skipping') === 0){
						logger.info(err);
						res.send(403, err);
					} else if(err.indexOf('Missing file') === 0){
						logger.info("Missing file %s", filePath);
						res.send(404, err);
					} else {
						throw err;
					}
				} else {
					throw err;
				}
			});
	}

	promise
		.then(function(){
			if(!res.headersSent){
				res.send(204);
			}
		})
		.fail(function(err){
			res.send(500, err);
		});
});

apiServer.get({ path: "/files", name: "getFiles" }, function(req, res){
	bundleManager.findFiles({ scanned: true, local: true, limit: 32 })
		.then(function(filePaths){
			//i love functional programming
			res.send(filePaths.map(encodeURI));
		})
		.fail(function(err){
			res.send(500, err);
		});
});

apiServer.get({ path: /\/files\/(.+)/, name: "getFile" }, function(req, res){
	res.setHeader('Content-Type', 'text/plain');

	var filePath = decodeURI(req.params[0]);
	fileManager.pipe(filePath, res)
		.then(function(){
			res.end();
		})
		.fail(function(err){
			if(err == 'not_found'){
				res.send(404);
			} else if(err == 'forbidden'){
				res.send(403);
			} else {
				res.send(500, err);
			}
		});
});

apiServer.post({ path: "/files/batchDelete", name: "batchDelete" }, function(req, res){
	var filePaths = _.map(req.body, decodeURI);
	fileManager.delete(filePaths)
		.then(function(){
			res.send(204);
		})
		.fail(function(err){
			res.send(500, err);
		});
});