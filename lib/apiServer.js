var config   = require('./config');
var logger   = require('./logger')(__filename);
var path     = require('path');
var Q        = require('q');
var restify  = require('restify');
var socketio = require('socket.io');
var zlib     = require('zlib');

var TRAILING_SLASH_PATTERN = /[^\/](\/+)$/;

var server = module.exports = restify.createServer();

server.pre(function(req, res, next){
	panicDbg.set('rollway.api.last-request', {
		client  : req.connection.remoteAddress,
		time    : req.time(),
		method  : req.method,
		url     : req.url,
		headers : req.headers
	});
	if(req.url != '/ruok'){
		logger.log("%s %s", req.method, req.url);
	}
	return next();
});

server.pre(function(req, res, next){
	req.connection.setTimeout(15*1000);
	var listener = function(){
		req.connection.destroy();
	};
	server.once('destroyConnections', listener);
	if(req.connection.listeners('timeout').length <= 1){
		req.connection.once('timeout', function(){
			req.connection.destroy();
			server.removeListener('destroyConnections', listener);
		});
	}
	return next();
});

server.on('uncaughtException', function(req, res, route, err){
	logger.error("Uncaught exception: %s", err);
	logger.error({
		client  : req.connection.remoteAddress,
		time    : req.time(),
		method  : req.method,
		url     : req.url,
		headers : req.headers
	});
});

/**
 * We are adding a listener per request so we can gracefully and quickly 
 * shut down in the face of idle keepalive connections.
 * To avoid leaking memory from all these listeners, each listener is 
 * removed when its connection times out.
 */
server.setMaxListeners(0);

server.pre(redirectToStripTrailingSlash);
server.pre(restify.pre.sanitizePath());
server.use(restify.queryParser());
server.use(restify.gzipResponse());
server.pre(restify.pre.userAgentConnection());
server.use(restify.bodyParser({ mapParams: false }));
server.use(errorLogger);

server.get({ path: 'ruok', name: 'heartbeat' }, function(req, res, next){
	res.send(204);
});


module.exports.start = function(){
	var deferred = Q.defer();
	var promise = deferred.promise;
	server.listen(config.apiPort, function(err){
		if(err != null){
			deferred.reject(err);
		} else {
			startSocketIO();
			deferred.resolve();
		}
	});

	server.on('error', deferred.reject);

	promise.then(
		function(){
			logger.info("Listening on %s", server.url);
		},
		function(err){ 
			if(err.code == 'EACCES'){
				logger.error("No access to port %d.", config.apiPort);
			} else if(err.code == 'EADDRINUSE'){
				logger.error("Port %d already in use.", config.apiPort);
			} else {
				logger.error("Error starting server: "+err.message); 
			}
			throw err;
		}
	);

	return promise;
};

function startSocketIO(){
	var io = server.io = socketio.listen(server, {
		'log level': 1, // 0 - error, 1 - warn, 2 - info, 3 - debug
		'transports': ['websocket', 'flashsocket', 'htmlfile', 'xhr-polling', 'jsonp-polling']
	});
	io.enable('browser client minification');
	io.enable('browser client etag');
	io.enable('browser client gzip');

	server.emit('sockets_listening');
}

module.exports.shutdown = function(){
	var deferred = Q.defer();
	try {
		server.emit('destroyConnections');
		server.close(function(err){
			if(err != null){
				logger.error("Unable to close: %s", err);
				deferred.reject(err);
			} else {
				logger.log("Shut down.");
				deferred.resolve();
			}
		});
	} catch (e){
		if(e == 'Error: Not running'){
			logger.log("Shut down.");
			deferred.resolve();
		} else {
			logger.error(e);
			deferred.reject(e);
		}
	}
	return deferred.promise;
};



function errorLogger(req, res, next){
	var originalEnd = res.end.bind(res);

	res.end = function(){
		if(res.statusCode >= 500){
			logger.error(res.statusCode + ": "+(res._body.stack || res._body));
		}
		originalEnd.apply(arguments);
	};

	next();
}

function redirectToStripTrailingSlash(req, res, next){
	var trailingSlashes = TRAILING_SLASH_PATTERN.exec(req.url);
	if(trailingSlashes !== null){
		res.setHeader('location', req.url.slice(0, -1 * trailingSlashes[1].length));
		res.send(301);
	} else {
		next();
	}
}