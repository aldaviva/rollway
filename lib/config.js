var _         = require('lodash');
var commander = require('commander');
var fs        = require('fs');
var logger    = require('./logger')(__filename);

// This should match config.json.example
var config = module.exports = {
	dataDir         : "/home/denimuser/logs/",
	apiPort         : 8080,
	dbPort          : 27017,
	dbHost          : "localhost",
	dbName          : "rollway",
	dbNameAnalytics : "rollway-analytics",
	logFile         : null,
	logLevel        : "info",
	uid             : null,
	cores           : true,
	smtpHost        : '127.0.0.1',
	smtpPort        : 25,
	smtpFrom        : 'logupload@bluejeans.com'
};

var isInitialized = false;

module.exports.init = function(config_){
	if(!isInitialized){
		isInitialized = true;

		if(config_){
			copyConfig(config_);
			applyLogConfig();
			logger.info("Using programmatic configuration.");
			onLoadedConfig();
		} else {
			commander
				.option('-c, --config [file]', 'Configuration file')
				.parse(process.argv);

			var filename = commander.config || "./config.json";

			if(fs.existsSync(filename)){
				var configText = fs.readFileSync(filename, 'utf8');
				try {
					var configObj = JSON.parse(configText);
					copyConfig(configObj);
				} catch(e){
					logger.error("%s in %s", e.message, filename);
				}

				applyLogConfig();
				logger.info("Using %s", filename);
				onLoadedConfig();

			} else {
				applyLogConfig();
				logger.info("Using defaults (missing %s)", filename);
				onLoadedConfig();
			}
		}
	}

	return config;
};

function applyLogConfig(){
	if(config.logFile){
		logger.useFile(config.logFile);
	}
	logger.setLevel(config.logLevel);

	require('./logger')('Server').info("Rollway starting.");
}

function onLoadedConfig(){
	/*for(var key in config){
		if(config.hasOwnProperty(key) && key != 'init'){
			logger.log("%s = %s", key, config[key]);
		}
	}*/

	if(config.uid != null && process.setuid){
		try {
			process.setuid(config.uid);
			logger.info("setuid to %s", process.getuid());
		} catch(e){
			logger.error("Unable to setuid to %s: %s", config.uid, e);
		}
	}

	fs.mkdir(config.dataDir, function(err){
		if(err != null && err.code != 'EEXIST'){
			logger.warn(err);
		}
	});
}

function copyConfig(config_){
	_.extend(config, config_);
}