var analyticsManager = require('./analyticsManager');
var apiServer        = require('./apiServer');
var config           = require('./config');
var fs               = require('fs');
var logger           = require('./logger')(__filename);
var Q                = require('q');
var restify          = require('restify');

/*
 * This module registers a route to '/', which would override other
 * more-specific routes. Therefore, this module should be loaded after all other 
 * routes are already registered.
 * See server.js for route registration.
 */

var INDEX_FILE = 'ui/target/index.html';

/* 
 * index.html is served separately from all other UI resources because we want
 * to set a custom header for cache-control, so that versions of JS and CSS are 
 * always up-to-date.
 */

apiServer.get({ path: '/', name: "getUiIndex" }, function(req, res, next){
	serveFile(INDEX_FILE, res, next, { cacheControl: 'no-cache' });
	analyticsManager.publish(analyticsManager.event.client.pageLoaded);
});

apiServer.get({ path: '/analytics', name: 'getAnalyticsIndex' }, function(req, res, next){
	serveFile('ui/target/analytics/index.html', res, next, { cacheControl: 'no-cache' });
});

apiServer.get({ path: /\/.*/, name: "getUiResource" }, restify.serveStatic({
	directory: './ui/target',
	maxAge: 3600 // 1 hour
}));


function serveFile(filePath, res, next, opts){
	opts = opts || {};
	Q.nfcall(fs.stat, filePath)
		.then(function(stats){
			var fstream = fs.createReadStream(filePath);
			fstream.once('open', function (fd) {
				res.set('Content-Length', stats.size);
				res.set('Content-Type', 'text/html');
				res.set('Last-Modified', stats.mtime);
				if(opts.cacheControl){
					res.set('Cache-Control', opts.cacheControl);
				}
				res.writeHead(200);
				fstream.pipe(res, { end: true });
				fstream.once('end', function () {
					next(false);
				});
			});
		})
		.fail(function(err){
			logger.error('Could not serve %s', filePath, err);
		});
}