var analyticsManager = require('./analyticsManager');
var bundleManager    = require('./bundleManager');
var config           = require('./config');
var fs               = require('fs');
var logger           = require('./logger')(__filename);
var path             = require('path');
var Q                = require('q');
var readline         = require('readline');
var stream           = require('stream');
var vasync           = require('vasync');

var SCAN_WORKER_COUNT = 32;
var LEG_PATTERN = /\[IDENTIFICATION\] ParticipantGUID: (\w{8}-\w{4}-\w{4}-\w{4}-\w{12})/g;
var MEETING_PATTERN = /\[IDENTIFICATION\] MeetingGUID: \w+:\w+-(\w{8}-\w{4}-\w{4}-\w{4}-\w{12})/g;
var NOOP_WRITE_STREAM = new stream();
var TEXT_FILE_PATTERN = /[a-zA-Z]{8}/;

var parentPath = path.resolve(config.dataDir);
var scanWorkerPool = vasync.queue(scanFileWorker, SCAN_WORKER_COUNT);

exports.scanFile = function(filePath){
	var deferred = Q.defer();
	scanWorkerPool.push(filePath, deferred.makeNodeResolver());
	return deferred.promise;
};

function scanFileWorker(filePath, callback){
	var absolutePath = getAbsolutePath(filePath);

	var scanStart = new Date();

	var scanPromise = scanFileForGuids(absolutePath);

	scanPromise
		.then(function(guids){
			return bundleManager.addGuids(filePath, guids.meetings, guids.legs);
		})
		.then(function(catchmark){
			return addCatchmark(absolutePath, catchmark);
		})
		.then(function(){
			return bundleManager.setFileScanned(filePath);
		})
		.nodeify(callback);

	scanPromise.then(function(){
		var scanEnd = new Date();
		fs.stat(absolutePath, function(err, stats){
			analyticsManager.publish(analyticsManager.event.server.scanVelocity, {
				fileSizeBytes: stats.size,
				durationMillis: scanEnd - scanStart
			});
		});
	});
}

function scanFileForGuids(filePath){
	var deferred = Q.defer();
	var legMatcher = new RegExp(LEG_PATTERN);
	var meetingMatcher = new RegExp(MEETING_PATTERN);

	var results = {
		legs: [],
		meetings: []
	};

	/* WARNING
	 * This implementation of a file stream ignores the last line of the file.
	 * This happens because Readline only emits line events when linefeeds are read.
	 * If a GUID happens after the last \n, it will be ignored.
	 * To fix this, we would need another implementation of streaming lines of files.
	 */
	var fileStream = fs.createReadStream(filePath, { encoding: 'utf8' });
	var lineReader = readline.createInterface({
		input: fileStream,
		output: NOOP_WRITE_STREAM,
		terminal: false
	});

	lineReader.on('line', function(data){
		var matches;
		while((matches = legMatcher.exec(data)) !== null){
			results.legs.push(matches[1]);
		}
		while((matches = meetingMatcher.exec(data)) !== null){
			results.meetings.push(matches[1]);
		}
	});

	lineReader.on('close', function(){
		deferred.resolve(results);
	});

	return deferred.promise;
}

//TODO bundlemanager should be not calling this if the file has already been added, but this should not leave the file with two catchmarks
function addCatchmark(absolutePath, catchmark){
	var fd;
	var catchmarkBuffer = new Buffer("\nRollway catchmark: "+catchmark, 'utf8');
	var catchmarkLength = catchmarkBuffer.length;
	var tailBuffer = new Buffer(catchmarkLength);

	var shouldAddCatchmark = true;

	if(shouldAddCatchmark){
		return Q.nfcall(fs.open, absolutePath, 'a+')
			.then(function(_fd){
				fd = _fd;
				return Q.nfcall(fs.fstat, fd);
			})
			.then(function(stats){
				var fileSize = stats.size;
				return Q.nfcall(fs.read, fd, tailBuffer, 0, catchmarkLength, fileSize - catchmarkLength);
			})
			.then(function(){
				if(tailBuffer.toString() == catchmarkBuffer.toString()){
					logger.info("Catchmark already added, skipping %s", absolutePath);
					return Q.reject('already_marked');
					//catchmark already there
				} else {
					return Q.resolve();
					//different or missing catchmark
				}
			})
			.then(function(){
				return Q.nfcall(fs.write, fd, catchmarkBuffer, 0, catchmarkBuffer.length, null);
			})
			.then(function(){
				logger.info("Appended catchmark to %s", absolutePath);
			})
			.fail(function(err){
				if(err !== 'already_marked'){
					logger.error("Unable to add catchmark to %s: %s", absolutePath, err);
					throw err;
				}
			})
			.finally(function(){
				fd && fs.close(fd);
			});
	} else {
		return Q.resolve();
	}
}

exports.pipe = function(srcPath, destStream){
	var absolutePath = getAbsolutePath(srcPath);
	var promise;

	if(absolutePath){
		promise = Q.nfcall(fs.stat, absolutePath)
			.then(function(stats){
				if(stats.isFile()){
					var deferred = Q.defer();
					var srcStream = fs.createReadStream(absolutePath);
					srcStream.pipe(destStream, { end: false });
					srcStream.on('end', function(){
						deferred.resolve();
					});
					return deferred.promise;
				} else if(stats.isDirectory()){
					return Q.reject('forbidden');
				} else {
					logger.log("stat neither file nor directory");
					return Q.reject('unknown');
				}
			})
			.fail(function(err){
				logger.log("stat error: %s", err);
				return Q.reject('not_found');
			});
	} else {
		promise = Q.reject('forbidden');
	}

	return promise;
};

exports.delete = function(paths){
	Array.isArray(paths) || (paths = [paths]);

	logger.info("Deleting files %s", paths);
	var deletionPromises = paths.map(deleteOneFile);
	return Q.all(deletionPromises);
};

exports.getComments = function(filePath){
	var absolutePath = getAbsolutePath(filePath);

	return Q.nfcall(fs.readFile, absolutePath, 'utf8');
};

function deleteOneFile(filePath){
	var absolutePath = getAbsolutePath(filePath);
	if(absolutePath){
		return Q.nfcall(fs.unlink, absolutePath)
			.then(function(){
				logger.info("Deleted %s", filePath);
			})
			.fail(function(err){
				if(err.code == 'ENOENT'){
					logger.info('Ignoring missing file %s', filePath);
				} else {
					logger.warn("Unable to delete %s: %s", filePath, err);
					throw err;
				}
			})
			.then(function(){
				return bundleManager.setFileNotLocal(filePath);
			});
		
	} else {
		return Q.reject(filePath+" is not in "+parentPath);
	}
}

function getAbsolutePath(filePath){
	var childPath = path.resolve(path.join(parentPath, filePath));
	var childStartsWithParent = (childPath.indexOf(parentPath) === 0);

	if(childStartsWithParent){
		return childPath;
	} else {
		return null;
	}
}

module.exports.ensureTextFile = function(filePath){
	var absolutePath = getAbsolutePath(filePath);
	var buffer = new Buffer(1024);
	var fd;
	
	buffer.fill(0);
	
	return Q.nfcall(fs.open, absolutePath, 'r')
		.fail(function(err){
			if(err.code == 'ENOENT'){
				throw "Missing file "+filePath;
			} else {
				throw err;
			}
		})
		.then(function(_fd){
			fd = _fd;
			return Q.nfcall(fs.read, fd, buffer, 0, buffer.length, null);
		})
		.then(function(){
			if(!TEXT_FILE_PATTERN.test(buffer.toString())){
				throw 'Skipping non-text file '+filePath;
			}
		})
		.finally(function(){
			fd && fs.close(fd);
		});
};