var _                  = require('lodash');
var apiServer          = require('./apiServer');
var logger             = require('./logger')(__filename);
var preferencesManager = require('./preferencesManager');
var Q                  = require('q');

apiServer.get({ path: "/preferences", name: "getPreferences" }, function(req, res){
	preferencesManager.getPreferences()
		.then(function(prefs){
			res.send(200, prefs);
		})
		.fail(function(err){
			res.send(500, err);
		});
});

apiServer.get({ path: "/preferences/:id", name: 'getPreferenceById' }, function(req, res){
	var id = req.params.id;
	
	preferencesManager.getPreferenceById(id)
		.then(function(pref){
			res.send(200, pref);
		})
		.fail(function(err){
			res.send(404, "Could not find preferences for "+id);
		});
});

apiServer.put({ path: "/preferences/:id", name: 'setPreferenceById' }, function(req, res){
	var id = req.params.id;

	preferencesManager.setPreferenceById(id, req.body)
		.then(function(){
			res.send(204);
		})
		.fail(function(err){
			switch(err){
				case 'invalid_preference_id':
					res.send(404, "Invalid preference id "+id);
					break;
				case 'missing_version':
					res.send(400, "Missing mandatory request object field _version");
					break;
				case 'out_of_date':
					res.send(409, "Attempted to modify an outdated version of "+id);
					break;
				default:
					res.send(500, err);
					break;
			}
		});

});