var _      = require('lodash');
var colors = require('cli-color');
var fs     = require('fs');
var path   = require('path');
var util   = require('util');

require('d8');
require('d8/locale/en-US');

colors.noop = function(x){ return x; };

var COLUMN_SEPARATOR = " ♦ ";

var LEVELS = {
	'log'   : 0,
	'info'  : 1,
	'warn'  : 2,
	'error' : 3
};

var LEVEL_COLORS = {
	log   : colors.noop,
	info  : colors.blueBright,
	warn  : colors.noop,
	error : colors.noop
};

var LINE_COLORS = {
	log   : colors.noop,
	info  : colors.noop,
	warn  : colors.yellowBright.bgBlack,
	error : colors.redBright.bgBlack
};

var logFile = null;
var minAllowedLevel = LEVELS['log'];

var maxModuleNameLength = _(fs.readdirSync(__dirname))
	.map(function(name){
		return path.basename(name, '.js');
	})
	.pluck('length')
	.max()
	.value();

module.exports = function(filename){
	return {
		log     : function(){ processMessage('log',   filename, arguments); },
		info    : function(){ processMessage('info',  filename, arguments); },
		warn    : function(){ processMessage('warn',  filename, arguments); },
		error   : function(){ processMessage('error', filename, arguments); },
		useFile : function(file){
			if(logFile){
				fs.closeSync(logFile);
			}
			logFile = fs.openSync(file, 'a');
		},
		setLevel : function(level){
			if(LEVELS[level] != null){
				minAllowedLevel = LEVELS[level];
			} else {
				processMessage('error', __filename, ["Invalid log level %s. Valid log levels are log, info, warn, error", level]);
			}
		}
	};
};

function processMessage(level, fileName, formatAndReplacements){
	var isLevelAllowed = (LEVELS[level] >= minAllowedLevel);

	if(isLevelAllowed){
		var moduleName = path.basename(fileName, '.js');
		moduleName = moduleName.charAt(0).toUpperCase() + moduleName.substr(1);

		var formatted = {
			level           : (level+"    ").substr(0,5),
			//d8 ISO_8601 shows Z instead of the UTC offset, even if the shown time is not UTC
			date            : (new Date()).format('Y-m-d<T>H:i:sO'),
			moduleName      : (moduleName+"             ").substr(0,maxModuleNameLength),
			message         : util.format.apply(null, formatAndReplacements),
			columnSeparator : COLUMN_SEPARATOR
		};

		if(!logFile && (LINE_COLORS[level] == colors.noop)){
			formatted.level = LEVEL_COLORS[level](formatted.level);
			formatted.date = colors.white(formatted.date);
			formatted.moduleName = colors.white(formatted.moduleName);
			formatted.columnSeparator = colors.blackBright(formatted.columnSeparator);
		}

		var completeMessage = [
			formatted.level,
			formatted.date,
			formatted.moduleName, 
			formatted.message
		].join(formatted.columnSeparator);

		if(!logFile){
			completeMessage = LINE_COLORS[level](completeMessage);
		}

		if(logFile){
			var buffer = new Buffer(completeMessage+"\n");
			fs.write(logFile, buffer, 0, buffer.length, null);
		} else {
			console[level](completeMessage);
		}
	}
}