var startTime = new Date();

require('./util');
var fs          = require('fs');
var logger      = require('./logger')(__filename);
var panic       = require('panic');
var panicLogger = require('./logger')("PANIC");
var Q           = require('q');

/**
 * Create .pid file
 */
fs.writeFile(".pid", process.pid);

/**
 * Load configuration
 */
var config = require('./config').init();

/**
 * Panic logging
 */
if(config.cores){
	panic.enablePanicOnCrash();
	process.on('uncaughtException', function(err){
		panicLogger.error(err.stack ? err.stack : err);
	});
}

/**
 * Connect to database
 */
var db = require('./database');
var dbConnectionPromise = db.connect();

/**
 * API server starts listening
 */
var apiServer = require('./apiServer');
require('./fileApi');
require('./bundleApi');
require('./preferencesApi');
require('./analyticsApi'); //set up websockets after server is already listening
require('./uiServer'); //this must be registered after all other API methods because it uses wildcards
var apiServerStartPromise = apiServer.start();	

/**
 * Shutdown handling
 */
var shutdownPromise;
var shutdown = module.exports.shutdown = function(){
	if(!shutdownPromise){
		shutdownPromise = startedPromise
			.finally(function(){
				return Q.resolve()
					.then(apiServer.shutdown())
					.then(db.shutdown())
					.fail(function(err){
						logger.error(err);
						throw err;
					})
					.then(function(){
						logger.info("Rollway shut down.");
						fs.unlinkSync(".pid");
						process.exit(0);
					});
			});
	}
	return shutdownPromise;
};

process.on('SIGINT', shutdown);


/**
 * Startup complete
 */
var startedPromise = module.exports.startedPromise = Q.all([ dbConnectionPromise, apiServerStartPromise ])
	.then(function(){
		logger.info("Startup complete in %d ms.", (new Date() - startTime));
	})
	.fail(function(err){
		logger.error("Failed to start: %s", err);
		shutdown();
		throw err;
	});
	
startedPromise.done();