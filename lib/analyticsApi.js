var _                = require('lodash');
var analyticsManager = require('./analyticsManager');
var apiServer        = require('./apiServer');
var config           = require('./config');
var logger           = require('./logger')(__filename);

var analyticsSocket;

apiServer.on('sockets_listening', function(){
	analyticsSocket  = apiServer.io.of('/analytics');

	analyticsManager.subscribe('', function(data, channel){
		var topic = channel.namespace;
		analyticsSocket.emit(topic, data);
	});
});

apiServer.get({ path: "/analytics/reports/:period", name: "getReports" }, function(req, res){
	var periodName = req.params.period;
	var reports;
	var reportIds = [
		'event:client:fileViewed',
		'event:client:indigoLinked',
		'event:client:pageLoaded',
		'event:server:bytesProcessed',
		'event:server:fileProcessed',
		'event:server:scanTime',
		'event:server:scanVelocity'
	];

	analyticsManager.getReportsByPeriod(periodName)
		.then(function(reports_){
			reports = reports_;
		})
		.then(function(){
			_(reportIds).each(function(reportId){
				if(!_.some(reports, function(report){
					return report._id === (reportId+'_'+periodName);
				})){
					reports.push(analyticsManager.getEmptyReport(reportId, periodName));
				}
			});
		})
		.then(function(){
			res.send(reports);
		})
		.fail(function(err){
			res.contentType = 'text';
			if(err.indexOf('Invalid report period') === 0){
				res.send(404, err);
			} else {
				res.send(500, err);
			}
		});
});

apiServer.post({ path: /\/analytics\/event\/([\w:]+)(?:\/([\w\d\.]+))?/, name: "notifyEvent" }, function(req, res){
	var topic = req.params[0];
	var value = req.params[1];
	var data = (typeof value !== 'undefined')
		? { value: value }
		: undefined;

	analyticsManager.publish(topic, data);
	res.send(204);
});