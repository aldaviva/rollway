var _             = require('lodash');
var apiServer     = require('./apiServer');
var bundleManager = require('./bundleManager');
var logger        = require('./logger')(__filename);
var path          = require('path');
var util          = require('util');

apiServer.get({ path: "/bundles", name: "getBundles" }, function(req, res, next){
	var rawRange = req.header('range', 'items=0-31');
	var range = rawRange.match(/items=(\d+)-(\d+)/).slice(1);
	var limit = Math.max(1, range[1] - range[0] + 1);
	
	var rawSort = req.query['sort'] || '+timestamp';
	var sort = rawSort.split(/,/g).map(function(rawSortField){
		var isAscending = (rawSortField.charAt(0) != '-');
		return [rawSortField.substr(1), isAscending ? 1 : -1];
	});

	var filter = _.pick(req.query, 'email', 'timestamp');

	var totalCount;

	bundleManager.countBundles(filter)
		.then(function(count_){
			totalCount = count_;
		})
		.then(function(){
			return bundleManager.getBundles(filter, {
				skip: range[0],
				limit: limit,
				sort: sort
			});
		})
		.then(function(bundles){
			var minIndex = range[0];
			var maxIndex = Math.max(minIndex, minIndex + bundles.length - 1);
			res.header('Content-Range', util.format('%d-%d/%d', minIndex, maxIndex, totalCount));
			res.send(bundles);
		})
		.fail(function(err){
			res.send(500, err);
		});
});

apiServer.get({ path: /\/bundles\/([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})/, name: "getBundle" }, function(req, res){
	var guid = req.params[0];
	bundleManager.getBundle(guid)
		.then(function(bundle){
			if(bundle === null){
				res.send(404);
			} else {
				res.send(bundle);
			}
		})
		.fail(function(err){
			res.send(500, err);
		});
});