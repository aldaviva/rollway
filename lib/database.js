var config = require('./config');
var logger = require('./logger')(__filename);
var mongo  = require('mongodb');
var Q      = require('q');

var dbOptions = {
	journal: true,
	numberOfRetries: Number.POSITIVE_INFINITY
};

var serverOptions = {
	auto_reconnect: true
};

var server = new mongo.Server(config.dbHost, config.dbPort, serverOptions);
server.allServerInstances().forEach(function(serverInstance){
	serverInstance.dbInstances = serverInstance.dbInstances || [];
});

var db = module.exports = new mongo.Db(config.dbName, server, dbOptions);
var analyticsDb = module.exports.analytics = db.db(config.dbNameAnalytics);

var dbPromise;

module.exports.connect = function(){
	var bundleDbOpenPromise = Q.ninvoke(db, "open")
		.then(onConnect)
		.then(function(){
			onConnect(analyticsDb);
		});

	dbPromise = bundleDbOpenPromise
		.then(function(){
			analyticsDb.collection('events').ensureIndex({ timestamp: 1 }, { sparse: true, background: true, unique: false }, function(err){
				if(err){
					logger.error("Could not create index on analytics events collection", err);
				}
			});
		})
		.fail(function(err){
			logger.error(err.message);
		});

	return dbPromise;
};

module.exports.shutdown = function(){
	return dbPromise
		.then(function(){
			var deferred = Q.defer();
			db.close(deferred.makeNodeResolver());
			return deferred.promise;
		})
		.finally(function(){
			logger.log("Shut down.");
		});
};

function onConnect(db_){
	logger.info("Connected to %s:%d/%s", db_.serverConfig.host, db_.serverConfig.port, db_.databaseName);
}