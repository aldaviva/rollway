var _           = require('lodash');
var config      = require('./config');
var crypto      = require('crypto');
var db          = require('./database');
var fileManager = require('./fileManager');
var logger      = require('./logger')(__filename);
var path        = require('path');
var Q           = require('q');

// var BASEPATH_PATTERN = /(\w+)\/+(?:([A-Za-z0-9._%+-]+@(?:[A-Za-z0-9-]+\.)+(?:[A-Za-z]{2}|aero|asia|biz|com|coop|edu|gov|info|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|xxx))\/)?(\d+)/;
// var BASEPATH_PATTERN = /(\w+)\/+(?:(.+)\/)?([\d\.]+)/; //mis-parses paths with dotfiles
var BASEPATH_PATTERN = /(\w+)\/+(?:([^\/]+)\/)?([\d\.]+)/;
var HASH_TO_GUID_PATTERN = /(\w{8})(\w{4})(\w{4})(\w{4})(\w{12})/;
var HASH_TO_GUID_REPLACEMENT = "$1-$2-$3-$4-$5";

var bundles = db.collection("bundles");

/**
 * opts:
 *   - limit
 *   - skip
 *   - sort
 */
exports.getBundles = function(criteria, opts){
	var filter = createFilter(criteria);

	_.defaults(opts, {
		limit: 0,
		skip: 0,
		sort: [['timestamp', 1]]
	});

	return Q.ninvoke(bundles, 'find', filter, { _id: 1, endpoint: 1, email: 1, timestamp: 1 }, opts)
		.then(function(cursor){
			return Q.ninvoke(cursor, 'toArray');
		});
};

exports.countBundles = function(criteria){
	var filter = createFilter(criteria);
	return Q.ninvoke(bundles, 'count', filter);
};

exports.getBundle = function(catchmark){
	return Q.ninvoke(bundles, 'findOne', { _id: catchmark });
};

exports.setFileNotLocal = function(filePath){
	var catchmark = getCatchmark(filePath);
	var filename  = path.basename(filePath);

	return Q.ninvoke(bundles, 'update', { _id: catchmark, "files.name": filename }, { $set: { "files.$.local": false }})
		.then(function(){
			logger.log("set %s local=false", filename);
		});
};

exports.setFileScanned = function(filePath){
	var catchmark = getCatchmark(filePath);
	var filename  = path.basename(filePath);

	return Q.ninvoke(bundles, 'update', { _id: catchmark, "files.name": filename }, { $set: { "files.$.scanned": true }})
		.then(function(){
			logger.log("set %s scanned=true", filename);
		});
};

exports.addFile = function(filePath){
	var catchmark;
	return getOrCreateBundle(filePath)
		.then(function(bundle){
			var basename = path.basename(filePath);
			catchmark = bundle._id;

			var fileExists = bundle.files.filter(function(file){
				return file.name == basename;
			}).length > 0;

			if(!fileExists){
				return Q.ninvoke(bundles, 'update', { _id: catchmark }, {
					$push: { files: {
						name    : basename,
						local   : true,
						scanned : false
					}}
				});
			} else {
				throw "exists";
			}
		})
		.fail(function(err){
			if(err == 'exists'){
				logger.info("Skipping previously-added file %s", filePath);
			} else {
				logger.error("Unable to create bundle for file %s (%s)", filePath, err);
			}
			throw err;
		})
		.then(function(){
			logger.info("Inserted %s into bundle %s", filePath, catchmark);
			return catchmark;
		});
};

exports.addGuids = function(filePath, meetingGuids, endpointGuids){
	var catchmark = getCatchmark(filePath);
	return Q.ninvoke(bundles, "update", { _id: catchmark },
		{
			$addToSet: {
				legs: {
					$each: endpointGuids
				},
				meetings: {
					$each: meetingGuids
				}
			}
		})
		.then(function(){
			return catchmark;
		})
		.fail(function(err){
			logger.error('Unable to add GUIDs for %s: %s', filePath, err);
			throw err;
		});
};

exports.setComment = function(filePath, comment){
	var catchmark = getCatchmark(filePath);
	return Q.ninvoke(bundles, 'update', { _id: catchmark }, {
		$set: {
			comment: comment
		}
	});
};

/**
 * findFiles({ limit: 50, local: true, scanned: true })
 */
exports.findFiles = function(opts){
	return findFilesWithAggregation(opts);
};

function findFilesWithAggregation(opts){
	var criteria = {};
	(opts.local   !== undefined) && (criteria['files.local']   = opts.local);
	(opts.scanned !== undefined) && (criteria['files.scanned'] = opts.scanned);

	var pipeline = [{
		$unwind  : '$files' 
	},{
		$match   : criteria
	},{
		$limit   : opts.limit || 1024
	},{
		$project : { _id: 0, endpoint: 1, email: 1, timestamp: 1, filename: "$files.name" }
	}];

	return Q.ninvoke(bundles, "aggregate", pipeline)
		.then(function(results){
			return _.map(results, function(result){
				var formattedTimestamp = (result.endpoint == 'acid' || (result.timestamp != ~~result.timestamp))
					? result.timestamp.toFixed(6)
					: result.timestamp;
				return [result.endpoint, result.email, formattedTimestamp, result.filename].join('/');
			});
		})
		.fail(function(err){
			logger.error("failed to aggregate: %s", err);
			throw err;
		});
}

function getCatchmark(filePath){
	return getPathInfo(filePath).catchmark;
}

function getOrCreateBundle(filePath){
	var pathInfo = getPathInfo(filePath);

	return Q.ninvoke(bundles, 'findOne', { _id: pathInfo.catchmark }, { returnKey: true })
		.then(function(bundle){
			var bundleExists = (bundle !== null);

			if(!bundleExists){
				bundle = {
					_id       : pathInfo.catchmark,
					endpoint  : pathInfo.endpoint,
					email     : pathInfo.email,
					timestamp : pathInfo.timestamp,
					comment   : "",
					meetings  : [],
					legs      : [],
					files     : []
				};

				return Q.ninvoke(bundles, 'insert', bundle).get(0);
					
			} else {
				return Q.resolve(bundle);
			}
		});
}

var getPathInfo = module.exports.getPathInfo = function(filePath){
	var matches = path.normalize(filePath).match(BASEPATH_PATTERN);

	if(matches != null){
		var hash = crypto.createHash('md5');
		hash.update(matches[0], 'utf8');

		var catchmark = hash
			.digest('hex')
			.toLowerCase()
			.replace(HASH_TO_GUID_PATTERN, HASH_TO_GUID_REPLACEMENT);

		return {
			catchmark : catchmark,
			endpoint  : matches[1],
			email     : matches[2],
			timestamp : parseFloat(matches[3])
		};
	} else {
		return null;
	}
};

function createFilter(criteria){
	criteria = criteria || {};
	var rawFilter = {};

	criteria.email && (rawFilter.email = {
		$regex: '.*'+criteria.email+'.*',
		$options: 'i'
	});
	
	criteria.timestamp && (rawFilter.timestamp = {
		$gte: Number(criteria.timestamp),
		$lte: Number(criteria.timestamp) + 60*60*24
	});

	return rawFilter;
}