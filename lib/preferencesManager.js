var _      = require('lodash');
var db     = require('./database');
var logger = require('./logger')(__filename);
var Q      = require('q');

var DEFAULT_PREFERENCES = {

	uploadNotifications: {
		recipients: [],
		capsLock: false
	}

};

_.each(DEFAULT_PREFERENCES, function(item){
	item._version = 0;
});

var collection = db.collection('preferences');

module.exports.getPreferences = function(){
	return Q.ninvoke(collection, 'find')
		.then(function(cursor){
			return Q.ninvoke(cursor, 'toArray');
		})
		.then(function(arr){
			var ids = _.pluck(arr, '_id');
			var prefsObj = _.zipObject(ids,
				_.map(arr, function(item){
					return _.omit(item, '_id');
				})
			);
			_.defaults(prefsObj, DEFAULT_PREFERENCES);
			return prefsObj;
		});
};

module.exports.getPreferenceById = function(id){
	return Q.ninvoke(collection, 'findOne', { _id: id })
		.then(function(doc){
			return _.defaults(_.omit(doc, '_id'), DEFAULT_PREFERENCES[id], { _version: 0 });
		});
};

module.exports.setPreferenceById = function(id, body){
	var deferred = Q.defer();

	if(typeof DEFAULT_PREFERENCES[id] != 'undefined'){

		var version = body._version;

		if(typeof version == 'number'){
			var doc = _.pick(body, _.keys(DEFAULT_PREFERENCES[id]));
			doc._id = id;
			doc._version = version + 1;

			Q.ninvoke(collection, 'update', { _id: id, _version: version }, doc, { upsert: true })
				.then(function(){
					deferred.resolve();
				})
				.fail(function(err){
					if(err.code == 11000){
						deferred.reject('out_of_date');
					} else {
						deferred.reject(err);
					}
				});
		} else {
			deferred.reject('missing_version');
		}
	} else {
		deferred.reject("invalid_preference_id");
	}

	return deferred.promise;
};