Rollway
=======

Rollway is a server application that runs on the Blue Jeans Log Upload server. It monitors uploads of logs files, and allows clients to list, search, download, and delete log files.

## API Documentation

See `doc/index.html`

## Requirements 

* [Node](https://github.com/joyent/node/wiki/Installation) >= 0.8.0
* [MongoDB](http://www.mongodb.org/) >= 2.2.0

### Development Requirements

* [NPM](https://github.com/isaacs/npm) (bundled with Node by default)
* [Javascript Lint](https://github.com/davepacheco/javascriptlint)
* [Restdown](https://github.com/trentm/restdown)
* [node-jscoverage](https://github.com/visionmedia/node-jscoverage)

## Installation

From a built package, extract the archive and run

	$ sudo make install

which will copy the SysV init script to `/etc/init.d/rollway` and register it.

## Configuration

Rollway can be configured using 

* config.json in the installation directory
* config.json in a different directory, if you launch Rollway with `--config [filename]`
* defaults otherwise

A default configuration file is provided in `config.json.example`.

#### Configuration options

* **dataDir** path containing Blue Jeans logs (should have Browser and acid as subdirectories) *example: /home/denimuser/logs/*
* **apiPort** port the Rollway REST API and HTTP interface should listen on *example: 8080*
* **dbPort** port Mongo is listening on *example: 27017*
* **dbHost** hostname Mongo is listening on *example: localhost*
* **dbName** name of the database in Mongo to use *example: rollway*
* **logFile** file to write Rollway logs to, or null to write to stdout *example: /var/log/rollway*
* **logLevel** how verbosely to log Rollway events *examples: log, info, warn, error*
* **uid** setuid to this uid or username to prevent Rollway from running as root, or null to skip *example: www-data*
* **cores** whether or not to [dump a hot, steaming core](https://github.com/joyent/node-panic) in the event of a crash *example: true*

## Running

Start Rollway:

	$ sudo service mongodb start
	$ sudo service rollway start

Stop Rollway:
	
	$ sudo service rollway stop
	
Restart Rollway:
	
	$ sudo service rollway restart
	
Check if Rollway is running:

	$ sudo service rollway status

## Development

Rollway is built using

	$ make

### Packaging

You can generate a packaged-up Rollway instance, including dependencies, with

	$ make package

### Tests

Tests depend on [Javascript Lint](https://github.com/davepacheco/javascriptlint), which must be built from source, even on Windows. Have fun.

To execute the test suite, run

	$ make test

### Test Coverage

Test coverage report generation requires [node-jscoverage](https://github.com/visionmedia/node-jscoverage).

To generate the report, run

	$ make coverage

The report is saved in `coverage.html`.