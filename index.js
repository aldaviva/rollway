process.chdir(__dirname);

module.exports = process.env.ROLLWAY_COVERAGE
	? require('./lib-cov/server')
	: require('./lib/server');